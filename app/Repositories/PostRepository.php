<?php

namespace App\Repositories;

use App\Implementations\QueryBuilderImplementation;

class PostRepository extends QueryBuilderImplementation
{

    public function __construct()
    {
        $this->table = 'posts';
        $this->pk = 'post_id';
    }

}