<?php

namespace App\Repositories;

use App\Implementations\QueryBuilderImplementation;

use DB;

class NewImageRepository extends QueryBuilderImplementation
{
    public function __construct()
    {
        $this->table = 'news_images';
    }

    public function deleteAll($id)
    {
        try {
            return DB::connection($this->db)
                ->table($this->table)
                ->where('news_id', '=', $id)
                ->delete();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

}