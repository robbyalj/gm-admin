<?php

namespace App\Repositories;

use App\Implementations\QueryBuilderImplementation;
use App\Images_gallery;

use DB;

class GalleryImageFileRepository extends QueryBuilderImplementation
{
    public function __construct()
    {
        $this->table = 'images_galleries_files';
        $this->pk = 'image_gallery_file_id';
    }

    //overide
    public function insert(array $data)
    {
        try {
            DB::beginTransaction();
            
                $file = $data['images_galleries_files'];
                if($file){
                    foreach ($file as $key => $val) {
                        $data['images_galleries_files'][$key]['created_at'] = $data['created_at'];
                        $data['images_galleries_files'][$key]['created_by'] = $data['created_by'];
                        $data['images_galleries_files'][$key]['image_gallery_id'] = $data['image_gallery_id'];

                        //insert into image gallery file
                        $imagegalleryfileid[$key] = DB::table('images_galleries_files')
                        ->insertGetId($data['images_galleries_files'][$key], 'image_gallery_file_id');
                        $imagegalleryfileids[] = $imagegalleryfileid[$key];
                    }
                }
            DB::commit();
            return $imagegalleryfileids;
        } catch (Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }
    }

    public function deleteAll($id)
    {
        try {
            return DB::connection($this->db)
                ->table($this->table)
                ->where('image_gallery_id', '=', $id)
                ->delete();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

}