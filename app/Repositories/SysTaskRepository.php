<?php

namespace App\Repositories;

use App\Implementations\QueryBuilderImplementation;

class SysTaskRepository extends QueryBuilderImplementation
{

	public $fillable = ['module_id', 'task_name', 'created_at', 'created_by', 'updated_at', 'updated_by'];

    public function __construct()
    {
        $this->table = 'sys_tasks';
        $this->pk = 'task_id';
    }

}