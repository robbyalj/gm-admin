<?php

namespace App\Repositories;

use App\Implementations\QueryBuilderImplementation;

class UserRepository extends QueryBuilderImplementation
{
	public $fillable = ['name', 'username', 'email', 'password', 'group_id', 'activation_token', 'activated_at', 'created_at', 'updated_at'];

    public function __construct()
    {
        $this->table = 'users';
        $this->pk = 'user_id';
    }

}