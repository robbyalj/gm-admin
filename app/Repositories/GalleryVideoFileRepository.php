<?php

namespace App\Repositories;

use App\Implementations\QueryBuilderImplementation;
use App\Images_gallery;

use DB;

class GalleryVideoFileRepository extends QueryBuilderImplementation
{
    public function __construct()
    {
        $this->table = 'videos_galleries_files';
        $this->pk = 'video_gallery_file_id';
    }

    //overide
    public function insert(array $data)
    {
        try {
            DB::beginTransaction();

                $file = $data['videos_galleries_files'];
                if($file){
                    foreach ($file as $key => $val) {
                        $data['videos_galleries_files'][$key]['created_at'] = $data['created_at'];
                        $data['videos_galleries_files'][$key]['created_by'] = $data['created_by'];
                        $data['videos_galleries_files'][$key]['video_gallery_id'] = $data['video_gallery_id'];
                        //insert into image gallery file
                        $videogalleryfile = DB::table('videos_galleries_files')
                        ->insert($data['videos_galleries_files'][$key]);
                    }
                }
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }
    }

    public function deleteAll($id)
    {
        try {
            return DB::connection($this->db)
                ->table($this->table)
                ->where('video_gallery_id', '=', $id)
                ->delete();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

}