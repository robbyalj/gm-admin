<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table        = 'sys_roles';
    protected $primaryKey   = 'roles_id';

    /**
     * Get the user that owns the task.
     */
    public function tasks()
    {
        return $this->belongsTo('App\Model\System\Task', 'tasks_id','tasks_id');
    }

    /**
     * Get the user that owns the user.
     */
    public function group()
    {
        return $this->belongsTo('App\Model\UserGroup', 'users_id','group_id');
    }
}
