<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = 'sys_modules';
}
