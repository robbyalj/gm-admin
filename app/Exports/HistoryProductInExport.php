<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Modules\Transaction\Repositories\TransactionRepository;
use Maatwebsite\Excel\Concerns\ToModel;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Cell\Cell;


class HistoryProductInExport extends DefaultValueBinder implements WithCustomValueBinder, FromView, ShouldAutoSize
{
    public function __construct(array $filter)
    {

        $this->_transactionRepository = new TransactionRepository;
        $this->_filter = $filter;
    }

    // public function collection()
    // {
    //     // return User::all();
    //     if (!empty($this->_filter)) {
    //         $this->_filter['status'] = '1';
    //         $show_products = $this->_transactionRepository->getAllByParams($this->_filter);
    //     } else {
    //         $show_products = null;
    //     }

    //     return $show_products;
    // }
    public function bindValue(Cell $cell, $value)
    {
        if ($cell->getColumn() == 'C') {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        }
        if ($cell->getColumn() == 'D') {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        }

        if ($cell->getColumn() == 'E') {
            $cell->setValueExplicit($value, DataType::TYPE_STRING);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    public function view(): View
    {
        if (!empty($this->_filter)) {
            $this->_filter['status'] = '1';
            $show_products = $this->_transactionRepository->getAllByParams($this->_filter);
        } else {
            $show_products = null;
        }

        return view('exports.HistoryProductIn', [
            'products' => $show_products
        ]);
    }

}