<?php 
/** 
 * Log Helpers
 * 
 *
 * @author Diskominfoarpus
 *
 */
namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use DB;


class LogHelper{
    public function __construct()
    {
        $this->db = 'mysql';
    }

	/**
	 * Normalize params
	 *
	 * @param
	 * - param module  <string>
	 * - param data <string>
	 * - param slug <string>
	 * 
	 * @return string
	 *
	 */
	public function store($module, $data, $slug){
        $now = date('Y-m-d H:i:s');
        $id  = Auth::user()->user_id;
        $name  = Auth::user()->name;
        if($slug == 'create'){
            $key = "Menambahkan";
        }elseif($slug == 'update') {
            $key = "Merubah";
        }else{
            $key = "Menghapus";
        }        
        
        $description = $name.' '.$key.' '.$module.' - '.$data;

        try {
            return DB::connection($this->db)
                ->table('log_activities')
                ->insert(['log_activity_desc' => $description,
                          'created_at' => $now,
                          'created_by' => $id
                        ]);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

}

?>