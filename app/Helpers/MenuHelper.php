<?php 
/** 
 * Menu Helper
 * 
 * Updated 11 Juni 2020, 09:40
 *
 * @author Yudi Setiadi Permana 
 *
 */
namespace App\Helpers;

use Request;
use Illuminate\Support\Facades\Auth;
use Modules\SysMenu\Repositories\SysMenuRepository;

class MenuHelper{

	/**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public static function render()
    {
    	$_sysmenuRepository = new SysMenuRepository;

        $getMenus   = $_sysmenuRepository->getAllOrderByParams(['menu_is_sub' => 0]);
        $menus      = "";

        if (sizeof($getMenus) > 0) {
            
            foreach ($getMenus as $menu) {
                    
                $getSubs    = $_sysmenuRepository->getAllOrderByParams(['menu_parent_id' => $menu->menu_id]);
                $subs       = "";
                $subLinks   = array();

                if (sizeof($getSubs) > 0) {

                    $areSubs = false;
                    
                    foreach ($getSubs as $sub) {

                        // Check Role
                        $getRole = $_sysmenuRepository->getRole($sub->module_id, Auth::user()->group_id);

                        if (!$getRole) {
                            continue;
                        }

                        $active = '';

                        if(Request::segment(1) == $sub->menu_url){
                            $active = 'active';
                        }

                        $subLinks[] = $sub->menu_url;
                        
                        $subs .= "
                            <li class='sidebar-item ". $active ."'>
                                <a href='". url($sub->menu_url) ."' class='sidebar-link ". $active ."'>
                                    <span class='hide-menu'>". $sub->menu_name ."</span>
                                </a>
                            </li>";

                        $areSubs = true;

                    }

                    if (!$areSubs) continue;

                    $selected   = '';
                    $in         = '';

                    if(in_array(Request::segment(1), $subLinks)){
                        $selected   = 'selected';
                        $in         = 'in';
                    }

                    $menus     .= "
	                    <li class='sidebar-item ". $selected ."'> 
	                        <a class='sidebar-link sidebar-link has-arrow' href='". $menu->menu_url ."' aria-expanded='false'>
	                            <i data-feather='". $menu->menu_icon ."' class='feather-icon'></i>
	                            <span class='hide-menu'>". $menu->menu_name ."</span>
	                        </a>
                            <ul aria-expanded='false' class='collapse first-level base-level-line ". $in ."'>
	                           ". $subs ."
                            </ul>
	                    </li>
	                ";

                }
                else{

                    // Check Role
                    $getRole = $_sysmenuRepository->getRole($menu->module_id, Auth::user()->group_id);

                    if (!$getRole) {
                        continue;
                    }

                    $active = '';

                    if(Request::segment(1) == $menu->menu_url){
                        $active = 'selected';
                    }

                	$menus     .= "
                    <li class='sidebar-item ". $active ."'> 
                        <a class='sidebar-link sidebar-link' href='". url($menu->menu_url) ."' aria-expanded='false'>
                            <i data-feather='". $menu->menu_icon ."' class='feather-icon'></i>
                            <span class='hide-menu'>". $menu->menu_name ."</span>
                        </a>
                    </li>
                ";

                }

            }

        }

        return $menus;
    }

}

?>