<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Helpers\DataHelper;
Use App\User;
use Session;
use AuthenticatesUsers;

class UserController extends Controller
{

    public function __construct(){

        $this->_userRepository = new UserRepository;

    }

    public function login()
    {	

    	if (Auth::check()) {
		    return redirect('dashboard');
		}

    	return view('user.login');
    }

    public function loginApi()
    {	

        dd('here');

    	if (Auth::check()) {
		    return redirect('dashboard');
		}

    	return view('user.login');
    }

    public function authenticate(Request $request)
    {

		$validator = Validator::make($request->all(), $this->_rules(), DataHelper::_rulesMessage())->validate();

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->intended('dashboard');
        }
        else{
        	return redirect('login')->with('error', 'Email atau kata sandi salah!');
        }

    }

    public function setting(){

        return view('user.setting');        

    }

    public function changepassword(Request $request){

        $currpass   = $request->input('currpass');
        $password   = $request->input('password');

        // if (Hash::check($currpass, Auth::user()->password)) {
        if (Hash::make($currpass) === Auth::user()->password) {
            return redirect('setting')->with('error', 'Kata sandi sekarang salah');
        }

        $this->_userRepository->update(DataHelper::_normalizeParams(['password' => $password], false, true), Auth::user()->user_id);

        return redirect('setting')->with('message', 'Kata sandi berhasil diubah');

    }

    public function logout()
    {
    	Auth::logout();

    	return redirect('login');
    }

    private function _rules(){

    	$rules = array(
    				'email' 	=> 'required|email',
		    		'password' 	=> 'required',
    			);

    	return $rules;

    }
}
