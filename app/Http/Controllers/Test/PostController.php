<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\PostRepository;

class PostController extends Controller
{

    public function __construct()
    {
        $this->_postRepository = new PostRepository;
    }

    public function index()
    {
        $posts = $this->_postRepository->getAll();

        return view('test.posts.index', compact('posts'));
    }

    public function store(Request $request)
    {
        $this->_postRepository->insert($request->all());
    }

    public function update($id, Request $request)
    {
        $this->_postRepository->update($request->all(), $id);
    }

    public function delete($id)
    {
        $this->_postRepository->delete($id);
    }

}
