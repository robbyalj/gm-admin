<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('transaction_id');
            $table->bigInteger('product_id')->unsigned();
            $table->Integer('recent_stock_a')->nullable();
            $table->Integer('input_stock_a')->nullable();
            $table->Integer('current_stock_a')->nullable();
            $table->Integer('recent_stock_b')->nullable();
            $table->Integer('input_stock_b')->nullable();
            $table->Integer('current_stock_b')->nullable();
            $table->smallInteger('status')->nullable();
            $table->dateTime('created_at');
            $table->bigInteger('created_by')->unsigned();
            $table->dateTime('updated_at')->nullable();
            $table->bigInteger('updated_by')->unsigned()->nullable();

            $table->foreign('product_id')
                ->references('product_id')
                ->on('products')
                ->onDelete('restrict');

            $table->foreign('created_by')
                ->references('user_id')
                ->on('users')
                ->onDelete('restrict');

            $table->foreign('updated_by')
                ->references('user_id')
                ->on('users')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
