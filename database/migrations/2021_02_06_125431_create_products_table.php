<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('product_id');
            $table->bigInteger('product_name')->unsigned();
            $table->bigInteger('product_variant_id')->unsigned();
            $table->bigInteger('product_size_id')->unsigned();
            $table->bigInteger('stock_a')->nullable();
            $table->bigInteger('stock_b')->nullable();
            $table->text('description')->nullable();
            $table->dateTime('created_at');
            $table->bigInteger('created_by')->unsigned();
            $table->dateTime('updated_at')->nullable();
            $table->bigInteger('updated_by')->unsigned()->nullable();

            $table->foreign('product_variant_id')
                ->references('product_variant_id')
                ->on('product_variants')
                ->onDelete('cascade');

            $table->foreign('product_size_id')
                ->references('product_size_id')
                ->on('product_sizes')
                ->onDelete('cascade');

            $table->foreign('created_by')
                ->references('user_id')
                ->on('users')
                ->onDelete('restrict');

            $table->foreign('updated_by')
                ->references('user_id')
                ->on('users')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
