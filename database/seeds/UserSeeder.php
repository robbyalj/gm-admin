<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'user_id'           => '1',
            'name' 				=> 'admin',
            'username'			=> 'admin',
            'email' 			=> 'admin@admin.com',
            'password' 			=> Hash::make('password'),
            'group_id'			=> '1',
            'activation_token'	=> md5(rand() . time()),
            'activated_at'      => date('Y-m-d H:i:s'),
            'created_at'		=> date('Y-m-d H:i:s'),
            'is_active'         => '1'
        ]);
    }
}
