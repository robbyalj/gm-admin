<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/', 'UserController@login');
Route::get('/login', 'UserController@login')->name('login');
Route::post('/do_login', 'UserController@authenticate');
Route::get('/logout', 'UserController@logout')->middleware('auth');
Route::get('/setting', 'UserController@setting')->middleware('auth');
Route::post('/changepassword', 'UserController@changepassword')->middleware('auth');

// Route::get('p', fn () => phpinfo());

Route::view('/unauthorize', 'exception.unauthorize');

Route::namespace('Test')->group(function() {
    Route::get('/posts', 'PostController@index');
    Route::post('/posts', 'PostController@store');
    Route::patch('/posts/{id}/edit', 'PostController@update');
    Route::delete('/posts/{id}/delete', 'PostController@delete');
});
