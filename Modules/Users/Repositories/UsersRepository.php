<?php

namespace Modules\Users\Repositories;

use App\Implementations\QueryBuilderImplementation;
use Illuminate\Support\Facades\DB;

class UsersRepository extends QueryBuilderImplementation
{

    public $fillable = ['name', 'username', 'email', 'password', 'group_id', 'activation_token', 'activated_at', 'created_at', 'updated_at'];

    public function __construct()
    {
        $this->table = 'users';
        $this->pk = 'user_id';
    }

    public function getAll()
    {
        try {
            return DB::connection($this->db)
                ->table($this->table)
                ->join('user_groups', 'user_groups.group_id', '=', 'users.group_id')
                ->get();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getById($id)
    {
        try {
            return DB::connection($this->db)
                ->table($this->table)
                ->join('user_groups', 'user_groups.group_id', '=', 'users.group_id')
                ->where($this->pk, '=', $id)
                ->first();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

}