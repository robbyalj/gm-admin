@extends('layouts.app')
@section('title', 'Pengguna')

@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Pengguna</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('') }}" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item"><a href="#" class="text-muted">Pengguna</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Daftar Pengguna</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    <!-- basic table -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4">
                    	<div class="col-md-6">
                    		<h4 class="card-title">Daftar Pengguna</h4>
                    	</div>
                    	<div class="col-md-6 text-right">
                    		<a href="javascript:void(0)" class="btn btn-primary btnAdd"><small class="fas fa-plus"></small> Tambah Pengguna</a>
                    	</div>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="20%">Nama</th>
                                    <th width="20%">Nama Pengguna</th>
                                    <th width="20%">Email</th>
                                    <th width="20%">Grup</th>
                                    <th width="15%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (sizeof($users) == 0)
                                	<tr>
                                		<td colspan="3" align="center">Data kosong</td>
                                	</tr>
                                @else
                                	@foreach ($users as $user)
                                		<tr>
                                			<td width="5%">{{ $loop->iteration }}</td>
                                			<td width="20%">{{ $user->name }}</td>
                                			<td width="20%">{{ $user->username }}</td>
                                			<td width="20%">{{ $user->email }}</td>
                                			<td width="20%">{{ $user->group_name }}</td>
                                			<td width="15%">
                                				<a href="javascript:void(0)" class="btn btn-sm btn-outline-secondary btnEdit" data-id="{{ $user->user_id }}">
                                					<i class="fas fa-pencil-alt"></i>
                                				</a>
                                				<a href="javascript:void(0)" class="btn btn-sm btn-outline-danger btnDelete" data-url="{{ url('users/delete/'. $user->user_id) }}">
                                					<i class="fas fa-trash-alt"></i>
                                				</a>
                                			</td>
                                		</tr>
                                	@endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<!-- Modal Add -->
<div class="modal addModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Pengguna</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ url('users/store') }}" method="POST" id="addForm">
                @csrf
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                        	<div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Masukan nama">
                                </div>
                            </div>
                        	
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="email" id="email" placeholder="Masukan email">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        	<div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama Pengguna</label>
                                    <input type="text" class="form-control" name="username" id="username" placeholder="Masukan nama pengguna">
                                </div>
                            </div>
                        	<div class="col-md-6">
                                <div class="form-group">
                                    <label>Grup</label>
                                    <select class="form-control" name="group_id" id="group_id">
                                    	<option value="">- Pilih Grup -</option>
                                    	@if(sizeof($groups) > 0) 
                                    		@foreach($groups as $group)
                                    			<option value="{{ $group->group_id }}">{{ $group->group_name }}</option>
                                    		@endforeach
                                    	@endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        	<div class="col-md-6">
                                <div class="form-group">
                                    <label>Kata Sandi</label>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Masukan kata sandi">
                                </div>
                            </div>
                        	<div class="col-md-6">
                                <div class="form-group">
                                    <label>Ulangi Kata Sandi</label>
                                    <input type="password" class="form-control" id="repassword" placeholder="Masukan ulang kata sandi">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Add -->

<script type="text/javascript">
	$('.btnAdd').click(function(){
        $('#name').val('');
        $('#email').val('');
        $('#password').val('');
        $('#repassword').val('');
        $('#group_id').val('');
        $('#username').val('');
        $('.addModal form').attr('action', "{{ url('users/store') }}");
        $('.addModal .modal-title').text('Tambah Pengguna');
		$('.addModal').modal();
	});

    $('.btnEdit').click(function(){

        var id  = $(this).attr('data-id');
        var url = "{{ url('users/getdata') }}";

        $('.addModal form').attr('action', "{{ url('users/update') }}" +'/'+ id);

        $.ajax({
            type : 'GET',
            url : url +'/'+ id,
            dataType : 'JSON',
            success : function(data) {
                console.log(data);

                if (data.status == 1) {

                    $('#name').val(data.result.name);
			        $('#email').val(data.result.email);
			        $('#password').val(data.result.password);
			        $('#group_id').val(data.result.group_id);
			        $('#username').val(data.result.username);
                    $('.addModal .modal-title').text('Ubah Pengguna');
                    $('.addModal').modal();

                }
                        
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                alert('Error : Gagal mengambil data'); 
            }
        });

    });

    $("#addForm").validate( {
        rules: {
            name: "required",
            email: {
            	required: true,
            	email: true
            },
            password: "required",
            repassword: {
            	equalTo: "#password"
            },
            group_id: "required",
            username: "required"
        },
        messages: {
            name: "Nama tidak boleh kosong",
            email: {
            	required: "Email tidak boleh kosong",
            	email: "Format email tidak valid"
            },
            password: "Kata sandi tidak boleh kosong",
            repassword: {
            	equalTo: "Ulang kata sandi tidak sesuai"
            },
            group_id: "Grup harus dipilih",
            username: "Kata sandi tidak boleh kosong"
        },
        errorElement: "em",
        errorClass: "invalid-feedback",
        errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            $(element).parents('.form-group').append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).addClass("is-invalid").removeClass("is-valid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).addClass("is-valid").removeClass("is-invalid");
        }
    });
</script>
@endsection