@extends('layouts.app')
@section('title', 'Produk')
<?php use App\Helpers\DateFormatHelper; ?>

@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Produk Keluar Gudang Kecil</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('') }}" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">produk keluar gudang kecil</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    <!-- basic table -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4">
                    	<div class="col-md-6">
                    		<h4 class="card-title">Cari Produk</h4>
                    	</div>
                    </div>
                    <form action="{{ url('productoutgk') }}" method="GET" id="addForm">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Nama Produk <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="product_name" id="product_name" placeholder="Masukan nama produk">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Varian <span class="text-danger">*</span></label>
                                        <select class="form-control" name="product_variant">
                                            <option value="">- Pilih Semua -</option>
                                            @if(sizeof($product_variants) > 0)

                                                @foreach($product_variants as $pv)
                                                    <option value="{{ $pv->product_variant_id }}">{{ $pv->product_variant }}</option>
                                                @endforeach

                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Ukuran <span class="text-danger">*</span></label>
                                        <select class="form-control" name="product_size">
                                            <option value="">- Pilih Semua -</option>
                                            @if(sizeof($product_sizes) > 0)

                                                @foreach($product_sizes as $ps)
                                                    <option value="{{ $ps->product_size_id }}">{{ $ps->product_size }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>.</label>
                                        <button type="submit" class="form-control btn btn-primary"><i class="fas fa-search-plus"></i> Cari</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4">
                    	<div class="col-md-6">
                    		<h4 class="card-title">Daftar Produk</h4>
                    	</div>
                    </div>
                    <form action="{{ url('productoutgk/store') }}" method="POST">
                        @csrf
                        <div class="table-responsive">
                            <table id="table_no_pag" class="table table-striped table-bordered no-wrap">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th width="30%">Nama</th>
                                        <th width="25%">Varian</th>
                                        <th width="10%">Ukuran</th>
                                        {{-- @if ($group > 1)
                                            @if ($group == 2) --}}
                                        {{-- <th width="15%">Gudang Besar</th> --}}
                                            {{-- @else --}}
                                                <th width="15%">Gudang Kecil</th>
                                            {{-- @endif
                                        @else - --}}
                                            {{-- <th width="15%">Gudang Besar</th>
                                            <th width="15%">Gudang Kecil</th> --}}
                                        {{-- @endif --}}
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($show_products == null)
                                        <tr>
                                            {{-- @if ($group > 1) --}}
                                            <td colspan="5" align="center">Data kosong</td>
                                            {{-- @else
                                                <td colspan="6" align="center">Data kosong</td>
                                            @endif --}}
                                        </tr>
                                    @else
                                        @foreach ($show_products as $sp)
                                            <tr>
                                                <td>{{ $loop->iteration }} <input type="hidden" class="form-control" name="products_id[]" value="{{$sp->product_id}}"></td>
                                                <td>{{ $sp->product_name }}</td>
                                                <td>{{ $sp->product_variant }}</td>
                                                <td>{{ $sp->product_size}}</td>
                                                {{-- @if ($group > 1)
                                                    @if ($group == 2)
                                                        <td><input type="hidden" name="recents_stock_a[]" value="{{$sp->stock_a}}"><input type="text" class="form-control" name="inputed_stock_a[]" placeholder="{{$sp->stock_a}}"></td>
                                                    {{-- @else --}}
                                                        <td><input type="hidden" name="recents_stock_b[]" value="{{$sp->stock_b}}"><input type="text" class="form-control" name="inputed_stock_b[]" placeholder="{{$sp->stock_b}}"></td>
                                                    {{-- @endif --}}
                                                {{-- @else --}}
                                                    {{-- <td>
                                                        <input type="hidden" name="recents_stock_a[]" value="{{$sp->stock_a}}">
                                                        <input type="text" class="form-control" name="inputed_stock_a[]" placeholder="{{$sp->stock_a}}">

                                                        <input type="hidden" name="recents_stock_b[]" value="{{$sp->stock_b}}">
                                                    </td> --}}
                                                {{-- @endif --}}
                                                
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <button type="button" class="btn btn-primary btnSimpan">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<script>

$("#addForm").validate( {
    rules: {
        product_name: "required",
    },
    messages: {
        product_name: "nama produk tidak boleh kosong",
    },
    errorElement: "em",
    errorClass: "invalid-feedback",
    errorPlacement: function ( error, element ) {
        // Add the `help-block` class to the error element
        $(element).parents('.form-group').append(error);
    },
    highlight: function ( element, errorClass, validClass ) {
        $( element ).addClass("is-invalid").removeClass("is-valid");
    },
    unhighlight: function (element, errorClass, validClass) {
        $( element ).addClass("is-valid").removeClass("is-invalid");
    }
});
</script>
@endsection