<?php

namespace Modules\ProductOutGk\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;

use Modules\Product\Repositories\ProductRepository;
use Modules\ProductVariant\Repositories\ProductVariantRepository;
use Modules\ProductSize\Repositories\ProductSizeRepository;
use Modules\Transaction\Repositories\TransactionRepository;
use App\Helpers\LogHelper;
use App\Helpers\DataHelper;
use Carbon\Carbon;
use PDF;
use DB;

class ProductOutGkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->module = "ProductOutGk";

        $this->_productRepository      = new ProductRepository;
        $this->_prodctSizeRepository  = new ProductSizeRepository;
        $this->_prodctVariantRepository  = new ProductVariantRepository;
        $this->_transactionRepository  = new TransactionRepository;

        $this->_logHelper              = new LogHelper;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        if(!empty($request->input('product_name'))){
            $filter['product_name'] = $request->input('product_name');
        }

        if(!empty($request->input('product_variant'))){
            $filter['product_variant_id'] = $request->input('product_variant');
        }

        if(!empty($request->input('product_size'))){
            $filter['product_size_id'] = $request->input('product_size');
        }

        $products       = $this->_productRepository->getAll();
        $product_sizes   = $this->_prodctSizeRepository->getAll();
        $product_variants   = $this->_prodctVariantRepository->getAll();

        if (!empty($filter)){
            $show_products = $this->_productRepository->getAllByParams($filter);
        }else{
            $show_products = null;
        }

        $group = Auth::user()->group_id;

        return view('productoutgk::index', compact('products', 'product_sizes', 'product_variants', 'show_products', 'group'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('productoutgk::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
         // Authorize
         if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $product_length = count($request->products_id);
        DB::beginTransaction();

        for ($i=0; $i < $product_length; $i++) {
            if($request->inputed_stock_b[$i] != null ){
                $current_b = $request->recents_stock_b[$i]-$request->inputed_stock_b[$i];
                
                $request['product_id']      = $request->products_id[$i];
                $request['recent_stock_b']  = $request->recents_stock_b[$i];
                $request['input_stock_b']   = $request->inputed_stock_b[$i];
                $request['current_stock_b'] = $current_b;
                $request['status']          = '2';
                $this->_transactionRepository->insert(DataHelper::_normalizeParams($request->all(), true));

                $id = $request['product_id'];

                $this->_productRepository->updateStockB(DataHelper::_normalizeParams($request->all(), false, true), $id);
            }        
            
        }

            $this->_logHelper->store($this->module, $request->product_name, 'create');
        
        DB::commit();

        return redirect('productoutgk')->with('message', 'Product berhasil dikeluarkan');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('productoutgk::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('productoutgk::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
