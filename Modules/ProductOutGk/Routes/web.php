<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('productoutgk')->group(function() {
    Route::get('/', 'ProductOutGkController@index');
    Route::get('/create', 'ProductOutGkController@create');
    Route::get('/show/{id}', 'ProductOutGkController@show');
    Route::get('/edit/{id}', 'ProductOutGkController@edit');
    Route::post('/store', 'ProductOutGkController@store');
    Route::post('/update/{id}', 'ProductOutGkController@update');
    Route::get('/delete/{id}', 'ProductOutGkController@destroy');
    Route::get('/getdata/{id}', 'ProductOutGkController@getdata');
    Route::post('/print', 'ProductOutGkController@print');
});
