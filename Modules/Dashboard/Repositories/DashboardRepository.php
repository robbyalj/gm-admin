<?php

namespace Modules\Dashboard\Repositories;

use App\Implementations\QueryBuilderImplementation;
use DB;

class DashboardRepository extends QueryBuilderImplementation
{

	public $fillable = ['product_id', 'recent_stock_a', 'input_stock_a', 'current_stock_a', 'recent_stock_b', 'input_stock_b', 'current_stock_b', 'status', 'created_by', 'created_at', 'updated_by', 'updated_at'];

    public function __construct()
    {
        $this->table = 'transactions';
        $this->pk = 'transaction_id';
    }

    public function getAllByParams(array $params)
    {
        try {
            $query =  DB::connection($this->db)
                ->table($this->table)
                ->select('transaction_id', 'recent_stock_a', 'input_stock_a', 'current_stock_a', 'recent_stock_b', 'input_stock_b', 'current_stock_b', 'product_name', 'product_size', 'product_variant', 'name', 'transactions.created_at')
                ->join('products', 'products.product_id', '=', 'transactions.product_id')
                ->join('product_sizes', 'products.product_size_id', '=', 'product_sizes.product_size_id')
                ->join('product_variants', 'products.product_variant_id', '=', 'product_variants.product_variant_id')
                ->join('users', 'transactions.created_by', '=', 'users.user_id')
                ->where(DB::raw('DATE(transactions.created_at)'), '>=', $params['start_date'])
                ->where(DB::raw('DATE(transactions.created_at)'), '<=', $params['finish_date'])
                ->where('transactions.status', $params['status'])
                ->orderBy('transactions.created_at', 'DESC');

            if (!empty($params['product_name'])) {
                $query->where('products.product_name', 'like', '%'. $params['product_name'] .'%');
            }
            if (!empty($params['product_variant_id'])) {
                $query->where('products.product_variant_id', $params['product_variant_id']);
            }

            if (!empty($params['product_size_id'])) {
                $query->where('products.product_size_id', $params['product_size_id']);
            }

            return $query->get();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

}