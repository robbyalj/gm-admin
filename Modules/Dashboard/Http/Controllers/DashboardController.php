<?php

namespace Modules\Dashboard\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

use Modules\Product\Repositories\ProductRepository;
use Modules\ProductVariant\Repositories\ProductVariantRepository;
use Modules\ProductSize\Repositories\ProductSizeRepository;
use Modules\Setting\Repositories\SettingRepository;
use Modules\Transaction\Repositories\TransactionRepository;
use App\Helpers\LogHelper;


class DashboardController extends Controller
{

    public function __construct()
    {
        // Require Login
        $this->middleware('auth');

        $this->_productRepository = new ProductRepository;
        $this->_prodctSizeRepository = new ProductSizeRepository;
        $this->_prodctVariantRepository = new ProductVariantRepository;
        $this->_transactionRepository = new TransactionRepository;
        $this->_settingRepository = new SettingRepository;


        $this->_logHelper = new LogHelper;
        $this->module = "Dashboard";
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $data['user'] = Auth::user()->name;
        $data['gudang_besar'] = $this->_productRepository->sum('1');
        $data['gudang_kecil'] = $this->_productRepository->sum('2');

        if ($data['gudang_besar'] != null && $data['gudang_kecil'] != null) {
            $data['total_barang'] = $data['gudang_besar']->stock_a + $data['gudang_kecil']->stock_b;
        } else {
            $data['total_barang'] = '0';
        }

        $best_seller = $this->_transactionRepository->getBestSellerByStat(0, 10);
        $best_seller_charts = $this->_transactionRepository->getBestSellerByStat(0, 5);


        $best_seller_merchant = $this->_transactionRepository->getBestSellerByMerchant(2);

        $count_seller_merchant = $this->_transactionRepository->getByMerchant(2);

        return view('dashboard::index', compact('data', 'best_seller', 'best_seller_merchant', 'count_seller_merchant', 'best_seller_charts'));
    }

    public function detail($name)
    {
        $product_stock = $this->_productRepository->stockProduct($name);
        $setting = $this->_settingRepository->getValue('MINIMUM_STOCK');

        return view('dashboard::detail', compact('product_stock', 'setting'));
    }



}