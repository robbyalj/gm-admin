@extends('layouts.app')
@section('title', 'Transaksi Produk Masuk')
<?php use App\Helpers\DateFormatHelper; ?>

@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Detail Best Seller</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('') }}" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">detail best seller</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    <!-- basic table -->
    <div class="row">
        
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center mb-4">
                        <h4 class="card-title">Product Out of Stock</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table no-wrap v-middle mb-0">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="60%">Produk</th>
                                    <th width="10%">Varian</th>
                                    <th width="10%">Ukuran</th>
                                    <th width="20%">Gudang</th>
                                    <!-- <th width="20%">Gudang Kecil</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                @if (sizeof($product_stock) == 0)
                                	<tr>
                                		<td colspan="4" align="center">Data kosong</td>
                                	</tr>
                                @else
                                	@foreach ($product_stock as $row)
                                		<tr>
                                			<td width="5%">{{ $loop->iteration }}</td>
                                            <td width="60%">{{ $row->product_name }}</td>
                                            <td width="10%">{{ $row->product_variant }}</td>
                                            <td width="20%">{{ $row->product_size}}</td>
                                			<td width="20%">
                                                @if($row->stock_a <= $setting->setting_value)
                                                    <span class="badge badge-danger">{{ $row->stock_a }}</span>
                                                @else
                                                    {{ $row->stock_a }}
                                                @endif         
                                            </td>
                                			<!-- <td width="20%">
                                                @if($row->stock_b <= 10)
                                                    <span class="badge badge-danger">{{ $row->stock_b }}</span>
                                                @else
                                                    {{ $row->stock_b }}
                                                @endif         
                                            </td> -->
                                		</tr>
                                	@endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> 
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
@endsection