@extends('layouts.app')

@section('title', 'Dashboard')

@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 align-self-center">
            <h3 class="page-title text-truncate text-dark font-weight-medium mb-1">Selamat Datang "{{$data['user']}}"</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- *************************************************************** -->
    <!-- Start First Cards -->
    <!-- *************************************************************** -->
    <div class="card-group">
        <!-- <div class="card border-right">
            <div class="card-body">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <div class="d-inline-flex align-items-center">
                            <h2 class="text-dark mb-1 font-weight-medium">{{$data['total_barang']}}</h2>
                        </div>
                        <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Total Produk</h6>
                    </div>
                    <div class="ml-auto mt-md-3 mt-lg-0">
                        <span class="opacity-7 text-muted"><i data-feather="file-text"></i></span>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="card border-right">
            <div class="card-body">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <div class="d-inline-flex align-items-center">
                            <h2 class="text-dark mb-1 font-weight-medium">{{$data['gudang_besar']->stock_a}}</h2>
                        </div>
                        <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Gudang</h6>
                    </div>
                    <div class="ml-auto mt-md-3 mt-lg-0">
                        <span class="opacity-7 text-muted"><i data-feather="align-left"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="card border-right">
            <div class="card-body">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <div class="d-inline-flex align-items-center">
                            <h2 class="text-dark mb-1 font-weight-medium">{{$data['gudang_kecil']->stock_b}}</h2>
                        </div>
                        <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Gudang Kecil</h6>
                    </div>
                    <div class="ml-auto mt-md-3 mt-lg-0">
                        <span class="opacity-7 text-muted"><i data-feather="info"></i></span>
                    </div>
                </div>
            </div>
        </div> -->
        {{--<div class="card">
            <div class="card-body">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <h2 class="text-dark mb-1 font-weight-medium">{{$data['count_activity']}}</h2>
                        <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Kegiatan</h6>
                    </div>
                    <div class="ml-auto mt-md-3 mt-lg-0">
                        <span class="opacity-7 text-muted"><i data-feather="calendar"></i></span>
                    </div>
                </div>
            </div>
        </div>--}}
    </div> 

    <!-- *************************************************************** -->
    <!-- Start News Highlight Table -->
    <!-- *************************************************************** -->
    <div class="row">
        {{-- <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center mb-4">
                        <h4 class="card-title">Product Out of Stock</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table no-wrap v-middle mb-0">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="60%">Produk</th>
                                    <th width="10%">Varian</th>
                                    <th width="10%">Ukuran</th>
                                    <th width="20%">Gudang Besar</th>
                                    <th width="20%">Gudang Kecil</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (sizeof($product_stock) == 0)
                                	<tr>
                                		<td colspan="4" align="center">Data kosong</td>
                                	</tr>
                                @else
                                	@foreach ($product_stock as $row)
                                		<tr>
                                			<td width="5%">{{ $loop->iteration }}</td>
                                            <td width="60%">{{ $row->product_name }}</td>
                                            <td width="10%">{{ $row->product_variant }}</td>
                                            <td width="20%">{{ $row->product_size}}</td>
                                			<td width="20%">
                                                @if($row->stock_a <= 10)
                                                    <span class="badge badge-danger">{{ $row->stock_a }}</span>
                                                @else
                                                    {{ $row->stock_a }}
                                                @endif         
                                            </td>
                                			<td width="20%">
                                                @if($row->stock_b <= 10)
                                                    <span class="badge badge-danger">{{ $row->stock_b }}</span>
                                                @else
                                                    {{ $row->stock_b }}
                                                @endif         
                                            </td>
                                		</tr>
                                	@endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>  --}}
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center mb-4">
                        <h4 class="card-title">Best Seller</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table no-wrap v-middle mb-0">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="55%">Produk</th>
                                    <th width="20%">Total Keluar</th>
                                    <th width="5%">Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (sizeof($best_seller) == 0)
                                	<tr>
                                		<td colspan="3" align="center">Data kosong</td>
                                	</tr>
                                @else
                                	@foreach ($best_seller as $row)
                                		<tr>
                                			<td width="5%">{{ $loop->iteration }}</td>
                                            <td width="40%">{{ $row->product_name }}</td>
                                            {{-- <td width="20%">
                                                @if($row->stock_gb <= 10)
                                                    <span class="badge badge-danger">{{ $row->stock_gb }}</span>
                                                @else
                                                    {{ $row->stock_gb }}
                                                @endif         
                                            </td>
                                			<td width="20%">
                                                @if($row->stock_gk <= 10)
                                                    <span class="badge badge-danger">{{ $row->stock_gk }}</span>
                                                @else
                                                    {{ $row->stock_gk }}
                                                @endif         
                                            </td> --}}
                                			<td width="20%">
                                                {{ $row->total }}    
                                            </td>
                                			<td width="20%">
                                                <a href="{{ url('dashboard/detail/'. $row->product_name) }}" class="btn btn-sm btn-outline-secondary">
                                                    <i class="fas fa-chart-line"></i>
                                                </a>   
                                            </td>
                                		</tr>
                                	@endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center mb-4">
                        <h4 class="card-title">Produk Keluar Berdasarkan Merchant</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table no-wrap v-middle mb-0">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="55%">Merchant</th>
                                    <th width="40%">Total Keluar</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (sizeof($best_seller_merchant) == 0)
                                	<tr>
                                		<td colspan="3" align="center">Data kosong</td>
                                	</tr>
                                @else
                                	@foreach ($best_seller_merchant as $row)
                                		<tr>
                                			<td width="5%">{{ $loop->iteration }}</td>
                                            <td width="40%">{{ $row->merchants_name }}</td>
                                			<td width="20%">
                                                {{ $row->total }}    
                                            </td>
                                		</tr>
                                	@endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center mb-4">
                        <h4 class="card-title">Best Seller Chart</h4>
                    </div>
                    <div id="chartdiv" style="width: 100%; height: 500px;"></div>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center mb-4">
                        <h4 class="card-title">Transaksi Keluar Berdasarkan Merchant</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table no-wrap v-middle mb-0">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="55%">Merchant</th>
                                    <th width="40%">Total Transaksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (sizeof($count_seller_merchant) == 0)
                                	<tr>
                                		<td colspan="3" align="center">Data kosong</td>
                                	</tr>
                                @else
                                	@foreach ($count_seller_merchant as $row)
                                		<tr>
                                			<td width="5%">{{ $loop->iteration }}</td>
                                            <td width="40%">{{ $row->merchants_name }}</td>
                                			<td width="20%">
                                                {{ $row->total }}    
                                            </td>
                                		</tr>
                                	@endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <!-- *************************************************************** -->
    <!-- End Top Leader Table -->
    <!-- *************************************************************** -->
    <!-- *************************************************************** -->
    <!-- Start News Highlight Table -->
    <!-- *************************************************************** -->
    {{--<div class="row">
        <div class="col-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center mb-4">
                        <h4 class="card-title">Pengumuman</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table no-wrap v-middle mb-0">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="60%">Judul</th>
                                    <th width="20%">Publikasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (sizeof($announcements) == 0)
                                	<tr>
                                		<td colspan="4" align="center">Data kosong</td>
                                	</tr>
                                @else
                                	@foreach ($announcements as $row)
                                		<tr>
                                			<td width="5%">{{ $loop->iteration }}</td>
                                            <td width="60%">{{ $row->title }}</td>
                                			<td width="20%">
                                                @if($row->is_published == 1)
                                                    <span class="badge badge-success">Ya</span>
                                                @else
                                                    <span class="badge badge-danger">Tidak</span>
                                                @endif         
                                            </td>
                                		</tr>
                                	@endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center mb-4">
                        <h4 class="card-title">Kegiatan</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table no-wrap v-middle mb-0">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="60%">Judul</th>
                                    <th width="20%">Publikasi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (sizeof($activities) == 0)
                                	<tr>
                                		<td colspan="4" align="center">Data kosong</td>
                                	</tr>
                                @else
                                	@foreach ($activities as $row)
                                		<tr>
                                			<td width="5%">{{ $loop->iteration }}</td>
                                            <td width="60%">{{ $row->title }}</td>
                                			<td width="20%">
                                                @if($row->is_published == 1)
                                                    <span class="badge badge-success">Ya</span>
                                                @else
                                                    <span class="badge badge-danger">Tidak</span>
                                                @endif         
                                            </td>
                                		</tr>
                                	@endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- *************************************************************** -->
    <!-- End Top Leader Table -->
    <!-- *************************************************************** -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
            
@endsection

@section('script')
<script src="https://cdn.amcharts.com/lib/5/index.js"></script>
<script src="https://cdn.amcharts.com/lib/5/percent.js"></script>
<script src="https://cdn.amcharts.com/lib/5/themes/Animated.js"></script>

<!-- Chart code -->
<script>
am5.ready(function() {

// Create root element
// https://www.amcharts.com/docs/v5/getting-started/#Root_element
var root = am5.Root.new("chartdiv");


// Set themes
// https://www.amcharts.com/docs/v5/concepts/themes/
root.setThemes([
  am5themes_Animated.new(root)
]);


// Create chart
// https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/
var chart = root.container.children.push(am5percent.PieChart.new(root, {
  layout: root.verticalLayout
}));


// Create series
// https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Series
var series = chart.series.push(am5percent.PieSeries.new(root, {
  valueField: "value",
  categoryField: "category"
}));

@foreach ($best_seller_charts as $best_seller_chart)

    console.log('{{$best_seller_chart->product_name}}');
    
@endforeach


// Set data
// https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Setting_data
series.data.setAll([
@foreach ($best_seller_charts as $best_seller_chart)

{ value: {{$best_seller_chart->total}}, category: '{{$best_seller_chart->product_name}}' },


@endforeach



]);


// Create legend
// https://www.amcharts.com/docs/v5/charts/percent-charts/legend-percent-series/
var legend = chart.children.push(am5.Legend.new(root, {
  centerX: am5.percent(50),
  x: am5.percent(50),
  marginTop: 15,
  marginBottom: 15
}));

legend.data.setAll(series.dataItems);


// Play initial series animation
// https://www.amcharts.com/docs/v5/concepts/animations/#Animation_of_series
series.appear(1000, 100);

}); // end am5.ready()
</script>
@endsection

