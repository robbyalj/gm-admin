<?php

namespace Modules\Product\Repositories;

use App\Implementations\QueryBuilderImplementation;
use DB;

class ProductRepository extends QueryBuilderImplementation
{

	public $fillable = ['product_id', 'product_name', 'product_name_id', 'product_variant_id', 'product_size_id', 'stock_a', 'stock_b', 'description', 'created_by', 'created_at', 'updated_by', 'updated_at'];

    public function __construct()
    {
        $this->table = 'products';
        $this->pk = 'product_id';
    }

    public function getAll(){
    	try {
            $query = DB::table($this->table)
                        ->select('product_id', 'product_names.product_name', 'products.product_name_id', 'products.product_variant_id', 'products.product_size_id', 'product_name', 'product_variant', 'product_size', 'stock_a', 'stock_b', 'products.description', 'products.created_by', 'products.created_at', 'products.updated_by', 'products.updated_at')
                        ->join('product_sizes', 'products.product_size_id', '=', 'product_sizes.product_size_id')
    					->join('product_variants', 'products.product_variant_id', '=', 'product_variants.product_variant_id')
    					->join('product_names', 'products.product_name_id', '=', 'product_names.product_name_id')
                        ->get();

    	} catch (Exception $e) {
    		die($e->getMessage());
    	}
    	return $query;
	}

    public function stockProduct($name){
    	try {
            $query = DB::table($this->table)
                        ->select('product_id', 'product_names.product_name', 'products.product_name_id', 'products.product_variant_id', 'products.product_size_id', 'product_name', 'product_variant', 'product_size', 'stock_a', 'stock_b', 'products.description', 'products.created_by', 'products.created_at', 'products.updated_by', 'products.updated_at')
                        ->join('product_sizes', 'products.product_size_id', '=', 'product_sizes.product_size_id')
    					->join('product_variants', 'products.product_variant_id', '=', 'product_variants.product_variant_id')
    					->join('product_names', 'products.product_name_id', '=', 'product_names.product_name_id')
                        ->where('product_names.product_name', '=', $name)
                        ->where(function($q) {
                            $q->where('products.stock_a', '<=', '10')
                              ->orWhere('products.stock_b', '<=', '10');
                        })
                        ->get();

    	} catch (Exception $e) {
    		die($e->getMessage());
    	}
    	return $query;
	}

    public function getAllByParams(array $params)
    {
        try {
            $query =  DB::connection($this->db)
                ->table($this->table)
                ->select('product_id', 'product_names.product_name', 'products.product_name_id', 'products.product_variant_id', 'products.product_size_id', 'product_name', 'product_variant', 'product_size', 'stock_a', 'stock_b', 'products.description', 'products.created_by', 'products.created_at', 'products.updated_by', 'products.updated_at')
                ->join('product_sizes', 'products.product_size_id', '=', 'product_sizes.product_size_id')
                ->join('product_variants', 'products.product_variant_id', '=', 'product_variants.product_variant_id')
    			->join('product_names', 'products.product_name_id', '=', 'product_names.product_name_id')
                ->orderBy('product_names.product_name', 'DESC');

            if (!empty($params['product_id'])) {
                $query->where('products.product_id', $params['product_id']);
            }

            if (!empty($params['product_name'])) {
                $query->where('product_names.product_name', 'like', '%'. $params['product_name'] .'%');
            }
            if (!empty($params['product_variant_id'])) {
                $query->where('products.product_variant_id', $params['product_variant_id']);
            }
            if (!empty($params['product_size_id'])) {
                $query->where('products.product_size_id', $params['product_size_id']);
            }

            return $query->get();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function sum($param)
    {
        try {
            $query = DB::connection($this->db)
                ->table($this->table);
                
                if ($param == '1') {
                    $query->select(DB::raw('SUM(stock_a) as stock_a'));
                }
                elseif ($param == '2') {
                    $query->select(DB::raw('SUM(stock_b) as stock_b'));
                }

                return $query->first();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function cekValidation($params)
    {
        try {
            $query = DB::table($this->table)
                ->select(DB::raw('COUNT(*) as count'))
                ->where('products.product_variant_id', $params['product_variant_id'])
                ->where('products.product_size_id', $params['product_size_id'])
                ->where('products.product_name_id', $params['product_name_id']);

                return $query->first();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function updateStockAB(array $data, $id)
    {
        try {
            DB::beginTransaction();
                DB::connection($this->db)
                ->table($this->table)
                ->where('product_id', $id)
                ->update(['stock_a' => $data['current_stock_a'], 'stock_b' => $data['current_stock_b'] ]);
            DB::commit();
            return $data;
        } catch (Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }
    }

    public function updateStockA(array $data, $id)
    {
        try {
            DB::beginTransaction();
                DB::connection($this->db)
                ->table($this->table)
                ->where('product_id', $id)
                ->update(['stock_a' => $data['current_stock_a']]);
            DB::commit();
            return $data;
        } catch (Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }
    }

    public function updateStockB(array $data, $id)
    {
        try {
            DB::beginTransaction();
                DB::connection($this->db)
                ->table($this->table)
                ->where('product_id', $id)
                ->update(['stock_b' => $data['current_stock_b'] ]);
            DB::commit();
            return $data;
        } catch (Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }
    }

}