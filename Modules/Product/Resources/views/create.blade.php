@extends('layouts.app')
@section('title', 'Tambah Produk')

@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Tambah Produk</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('') }}" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('product') }}" class="text-muted">Produk</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Tambah</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @if($errors->any())
        <div class="alert alert-danger">
            {{$errors->first()}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
    @endif
    <!-- basic table -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ url('product/store') }}" method="POST" id="addForm" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama Produk<span class="text-danger">*</span></label>
                                    <select class="form-control" name="product_name_id">
                                        <option value="">- Pilih Nama Produk -</option>
                                        @if(sizeof($product_names) > 0)

                                            @foreach($product_names as $pn)
                                                <option value="{{ $pn->product_name_id }}">{{ $pn->product_name }}</option>
                                            @endforeach

                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Varian <span class="text-danger">*</span></label>
                                    <select class="form-control" name="product_variant_id">
                                        <option value="">- Pilih Varian -</option>
                                        @if(sizeof($product_variants) > 0)

                                            @foreach($product_variants as $pv)
                                                <option value="{{ $pv->product_variant_id }}">{{ $pv->product_variant }}</option>
                                            @endforeach

                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Ukuran <span class="text-danger">*</span></label>
                                    <select class="form-control" name="product_size_id">
                                        <option value="">- Pilih Ukuran -</option>
                                        @if(sizeof($product_sizes) > 0)

                                            @foreach($product_sizes as $ps)
                                                <option value="{{ $ps->product_size_id }}">{{ $ps->product_size }}</option>
                                            @endforeach

                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Jumlah Gudang Besar <span class="text-danger"></span></label>
                                    <input type="text" class="form-control" name="stock_a" id="stock_a" placeholder="Masukan Jumlah Gudang besar">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Jumlah Gudang Kecil <span class="text-danger"></span></label>
                                    <input type="text" class="form-control" name="stock_b" id="stock_b" placeholder="Masukan jumlah gudang kecil">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary btnCancel">Batal</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>

<script type="text/javascript">
    $("#addForm").validate( {
        rules: {
            product_name: {
                required: true,
            },
            stock_a:  {
                number : true,
            },
            stock_b:  {
                number : true,
            },
            product_size_id : "required",
            product_variant_id : "required",
            product_name_id : "required"
        },
        messages: {
            product_name: "Nama Produk harus di isi dan tidak boleh menggunakan (slash)",
            stock_a: "Hanya berisi angka",
            stock_b: "Hanya berisi angka",
            product_size_id: "Harus dipilih",
            product_variant_id: "Harus dipilih",
            product_name_id: "Harus dipilih",
        },
        errorElement: "em",
        errorClass: "invalid-feedback",
        errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            $(element).parents('.form-group').append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).addClass("is-invalid").removeClass("is-valid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).addClass("is-valid").removeClass("is-invalid");
        }
    });
</script>
<!-- ============================================================== -->
<!-- End Container fluid  -->
@endsection