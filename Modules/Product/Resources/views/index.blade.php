@extends('layouts.app')
@section('title', 'Produk')
<?php use App\Helpers\DateFormatHelper; ?>

@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Daftar Produk</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('') }}" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">daftar produk</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    <!-- basic table -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4">
                    	<div class="col-md-6">
                    		<h4 class="card-title">Daftar Produk</h4>
                            {!! QrCode::size(100)->generate('https://techvblogs.com/blog/generate-qr-code-laravel-8') !!}
                    	</div>
                    	<div class="col-md-6 text-right">
                    		<a href="{{ url('product/create') }}" class="btn btn-primary"><small class="fas fa-plus"></small> Tambah Produk</a>
                    	</div>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="40%">Nama</th>
                                    <th width="10%">Varian</th>
                                    <th width="10%">Ukuran</th>
                                    <th width="10%">Gudang Besar</th>
                                    <th width="10%">Gudang Kecil</th>
                                    <th width="15%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (sizeof($products) == 0)
                                	<tr>
                                		<td colspan="7" align="center">Data kosong</td>
                                	</tr>
                                @else
                                	@foreach ($products as $product)
                                		<tr>
                                			<td width="5%">{{ $loop->iteration }}</td>
                                            <td width="40%">{{ $product->product_name }}</td>
                                            <td width="10%">{{ $product->product_variant }}</td>
                                            <td width="20%">{{ $product->product_size}}</td>
                                            <td width="10%">{{ $product->stock_a}}</td>
                                            <td width="1-%">{{ $product->stock_b}}</td>
                                			<td width="15%">
                                                <div class="row">
                                                    <a href="{{ url('product/print-qr/'. $product->product_id) }}" class="btn btn-sm btn-outline-warning">
                                                        <i class="fas fa-share"></i>
                                                    </a>
                                                    <a href="{{ url('product/edit/'. $product->product_id) }}" class="btn btn-sm btn-outline-secondary">
                                                        <i class="fas fa-pencil-alt"></i>
                                                    </a>
                                                    <a href="javascript:void(0)" class="btn btn-sm btn-outline-danger btnDelete" data-url="{{ url('product/delete/'. $product->product_id) }}">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </a>
                                                    
                                                </div>
                                			</td>
                                		</tr>
                                	@endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- Modal Print -->
    <div class="modal printModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cetak Laporan <small class="fas fa-print"></small></h5> 
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ url('sysmenu/store') }}" method="POST" id="addForm">
                    @csrf
                    <div class="modal-body">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Dari Tanggal <span class="text-danger">*</span></label>
                                        <input type="date" class="form-control" name="start_date" id="start_date">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Sampai Tanggal <span class="text-danger">*</span></label>
                                        <input type="date" class="form-control" name="finish_date" id="finish_date">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Urutan Berdasarkan <span class="text-danger">*</span></label>
                                        <select class="form-control" name="order_by" id="order_by">
                                            <option value="">- Pilih -</option>
                                            <option value="0">Tanggal Dibuat</option>
                                            <option value="1">Jumlah Hit</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tipe Urutan <span class="text-danger">*</span></label>
                                        <select class="form-control" name="order_type" id="order_type">
                                            <option value="">- Pilih -</option>
                                            <option value="0">Menaik (ASC)</option>
                                            <option value="1">Menurun (DESC)</option>                                        
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Cetak</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal Print -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<script>
$('.btnPrint').click(function(){
    $('#start_date').val('');
    $('#finish_date').val('');
    $('#order_by').val('');
    $('#order_type').val('');
    $('.printModal form').attr('action', "{{ url('product/print') }}");
    $('.printModal').modal();
});

$("#addForm").validate( {
    rules: {
        start_date: "required",
        finish_date: "required",
        order_by: "required",
        order_type: "required"
    },
    messages: {
        start_date: "tangaal mulai tidak boleh kosong",
        finish_date: "tanggal akhir tidak boleh kosong",
        order_by: "urutan berdasarkan tidak boleh kosong",
        order_type: "tipe urutan tidak boleh kosong"
    },
    errorElement: "em",
    errorClass: "invalid-feedback",
    errorPlacement: function ( error, element ) {
        // Add the `help-block` class to the error element
        $(element).parents('.form-group').append(error);
    },
    highlight: function ( element, errorClass, validClass ) {
        $( element ).addClass("is-invalid").removeClass("is-valid");
    },
    unhighlight: function (element, errorClass, validClass) {
        $( element ).addClass("is-valid").removeClass("is-invalid");
    }
});
</script>
@endsection