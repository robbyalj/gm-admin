<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('product')->group(function() {
    Route::get('/', 'ProductController@index');
    Route::get('/create', 'ProductController@create');
    Route::get('/show/{id}', 'ProductController@show');
    Route::get('/edit/{id}', 'ProductController@edit');
    Route::post('/store', 'ProductController@store');
    Route::post('/update/{id}', 'ProductController@update');
    Route::get('/delete/{id}', 'ProductController@destroy');
    Route::get('/getdata/{id}', 'ProductController@getdata');
    Route::post('/print', 'ProductController@print');
    Route::get('/print-qr/{id}', 'ProductController@printQr');
});
