<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

use Modules\Product\Repositories\ProductRepository;
use Modules\ProductVariant\Repositories\ProductVariantRepository;
use Modules\ProductSize\Repositories\ProductSizeRepository;
use Modules\ProductName\Repositories\ProductNameRepository;
use App\Helpers\LogHelper;
use App\Helpers\DataHelper;
use Storage;
use Carbon\Carbon;
use PDF;
use DB;
use QrCode;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->module = "Product";

        $this->_productRepository      = new ProductRepository;
        $this->_prodctSizeRepository  = new ProductSizeRepository;
        $this->_prodctVariantRepository  = new ProductVariantRepository;
        $this->_prodctNameRepository  = new ProductNameRepository;

        $this->_logHelper              = new LogHelper;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $products       = $this->_productRepository->getAll();

        return view('product::index', compact('products'));
    }

    public function indexApi()
    {
        dd('$product');

        $products       = $this->_productRepository->getAll();

        dd($product);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $product_sizes   = $this->_prodctSizeRepository->getAll();
        $product_variants   = $this->_prodctVariantRepository->getAll();
        $product_names   = $this->_prodctNameRepository->getAll();

        return view('product::create', compact( 'product_sizes', 'product_variants', 'product_names'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $param['product_variant_id'] = $request->input('product_variant_id');
        $param['product_size_id'] = $request->input('product_size_id');
        $param['product_name_id'] = $request->input('product_name_id');

        $count = $this->_productRepository->cekValidation($param);

        if($count->count > 0){
            // return redirect::back()->withErrors(['msg', 'Barang sudah terinput']);
            return back()->withErrors(['field_name' => ['Barang sudah terinput']]);
        }
        
        DB::beginTransaction();

            $this->_productRepository->insert(DataHelper::_normalizeParams($request->all(), true));

            $this->_logHelper->store($this->module, $request->product_name, 'create');
        
        DB::commit();

        return redirect('product')->with('message', 'Produk berhasil ditambahkan');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('product::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $detail  = $this->_productRepository->getById($id);

        if (!$detail) {
            return redirect('product');
        }

        $product_sizes   = $this->_prodctSizeRepository->getAll();
        $product_variants   = $this->_prodctVariantRepository->getAll();
        $product_names   = $this->_prodctNameRepository->getAll();

        return view('product::edit', compact('detail', 'product_sizes', 'product_variants', 'product_names'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function printQr($id)
    {
        $params['product_id'] = $id;
        $detail  = $this->_productRepository->getAllByParams($params);

        if (!$detail) {
            return redirect('product');
        }

        $product_names   = $detail[0]->product_name;
        $product_variants   = $detail[0]->product_variant;
        $product_sizes   = $detail[0]->product_size;

        //file names
        $fileName = $product_names.' '.$product_variants.' '.$product_sizes . '-' . time()  . '.png';
        $headers    = array('Content-Type' => ['png','svg','eps']);

        //string on qr code
        $url = strval($id);

        //generate QR code
        $qrcode = QrCode::format('png')
                    ->size(200)
                    ->errorCorrection('H')
                    ->generate($url);

        $output_file = 'public/img/qr-code/' . $fileName;

        //store image
        Storage::disk('local')->put($output_file, $qrcode);  
        
        return response()->download(storage_path("app/public/img/qr-code/{$fileName}"))->deleteFileAfterSend();
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        // Check detail to db
        $detail  = $this->_productRepository->getById($id);

        if (!$detail) {
            return redirect('product');
        }

        DB::beginTransaction();
            $this->_productRepository->update(DataHelper::_normalizeParams($request->all(), false, true), $id);

            $this->_logHelper->store($this->module, $request->product_name, 'update');
        DB::commit();

        return redirect('product')->with('message', 'Produk berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        // Check detail to db
        $detail  = $this->_productRepository->getById($id);

        if (!$detail) {
            
            return redirect('product');
        }


        DB::beginTransaction();
            $this->_productRepository->delete($id);
            $this->_logHelper->store($this->module, $detail->product_name_id, 'delete');
        DB::commit();

        return redirect('product')->with('message', 'Produk berhasil dihapus');
    }
}
