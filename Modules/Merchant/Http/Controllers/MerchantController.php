<?php

namespace Modules\Merchant\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

use Modules\Merchant\Repositories\MerchantRepository;
use App\Helpers\DataHelper;
use App\Helpers\LogHelper;
use DB;

class MerchantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->module = "Merchant";

        $this->_merchantRepository = new MerchantRepository;
        $this->_logHelper          = new LogHelper;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
       // Authorize
       if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $merchants = $this->_merchantRepository->getAll();
        
        return view('merchant::index', compact('merchants'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('merchant::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
         // Authorize
         if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $validated = $request->validate([
            'merchants_name' => 'required|unique:merchants,merchants_name',
        ],
        [
            'merchants_name.unique' => 'merchant sudah terinput',
        ]);

        DB::beginTransaction(); 
        $this->_merchantRepository->insert(DataHelper::_normalizeParams($request->all(), true));

        $this->_logHelper->store($this->module, $request->merchants_name, 'create');
        DB::commit();
        return redirect('merchant')->with('message', 'Merchant telah berhasil ditambahkan');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('merchant::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('merchant::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
         // Authorize
         if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $validated = $request->validate([
            'merchants_name' => 'required|unique:merchants,merchants_name,'.$id.',merchants_id',
        ],
        [
            'merchants_name.unique' => 'merchant sudah terinput',
        ]);

        // Check detail to db
        $detail  = $this->_merchantRepository->getById($id);

        if (!$detail) {
            return redirect('merchant');
        }
        DB::beginTransaction();
        
        $this->_merchantRepository->update(DataHelper::_normalizeParams($request->all(), false, true), $id);
        $this->_logHelper->store($this->module, $request->merchants_name, 'update');

        DB::commit();

        return redirect('merchant')->with('message', 'Merchant berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }
        
        // Check detail to db
        $detail  = $this->_merchantRepository->getById($id);

        if (!$detail) {
            return redirect('merchant');
        }

        DB::beginTransaction();

        $this->_merchantRepository->delete($id);
        $this->_logHelper->store($this->module, $detail->merchants_name, 'delete');

        DB::commit();

        return redirect('merchant')->with('message', 'Merchant berhasil dihapus');
    }

    /**
     * Get data the specified resource in storage.
     * @param int $id
     * @return Response
     */
    public function getdata($id){

        $response   = array('status' => 0, 'result' => array()); 
        $getDetail  = $this->_merchantRepository->getById($id);

        if ($getDetail) {
            $response['status'] = 1;
            $response['result'] = $getDetail;
        }

        return $response;
    }
}
