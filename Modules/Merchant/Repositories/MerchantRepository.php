<?php

namespace Modules\Merchant\Repositories;

use App\Implementations\QueryBuilderImplementation;
use DB;

class MerchantRepository extends QueryBuilderImplementation
{

	public $fillable = ['merchants_name', 'merchants_url', 'created_by', 'created_at', 'updated_by', 'updated_at'];

    public function __construct()
    {
        $this->table = 'merchants';
        $this->pk = 'merchants_id';
    } 

}