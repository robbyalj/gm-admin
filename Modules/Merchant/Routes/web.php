<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('merchant')->group(function() {
    Route::get('/', 'MerchantController@index');
    Route::get('/create', 'MerchantController@create');
    Route::get('/show/{id}', 'MerchantController@show');
    Route::get('/edit/{id}', 'MerchantController@edit');
    Route::post('/store', 'MerchantController@store');
    Route::post('/update/{id}', 'MerchantController@update');
    Route::get('/delete/{id}', 'MerchantController@destroy');
    Route::get('/getdata/{id}', 'MerchantController@getdata');
});
