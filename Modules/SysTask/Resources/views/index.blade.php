@extends('layouts.app')
@section('title', 'Task')

@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Task</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('') }}" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item"><a href="#" class="text-muted">Hak Akses</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Task</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    <!-- basic table -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4">
                    	<div class="col-md-6">
                    		<h4 class="card-title">Daftar Task</h4>
                    	</div>
                    	<div class="col-md-6 text-right">
                    		<a href="javascript:void(0)" class="btn btn-primary btnAdd"><small class="fas fa-plus"></small> Tambah Task</a>
                    	</div>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="40%">Module</th>
                                    <th width="40%">Task</th>
                                    <th width="15%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (sizeof($tasks) == 0)
                                	<tr>
                                		<td colspan="4" align="center">Data kosong</td>
                                	</tr>
                                @else
                                	@foreach ($tasks as $task)
                                		<tr>
                                			<td width="5%">{{ $loop->iteration }}</td>
                                			<td width="80%">{{ $task->module_name }}</td>
                                			<td width="80%">{{ $task->task_name }}</td>
                                			<td width="15%">
                                				<a href="javascript:void(0)" class="btn btn-sm btn-outline-secondary btnEdit" data-id="{{ $task->task_id }}">
                                					<i class="fas fa-pencil-alt"></i>
                                				</a>
                                				<a href="javascript:void(0)" class="btn btn-sm btn-outline-danger btnDelete" data-url="{{ url('systask/delete/'. $task->task_id) }}">
                                					<i class="fas fa-trash-alt"></i>
                                				</a>
                                			</td>
                                		</tr>
                                	@endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<!-- Modal Add -->
<div class="modal addModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Task</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ url('systask/store') }}" method="POST" id="addForm">
                @csrf
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                        	<div class="col-md-12">
                                <div class="form-group">
                                    <label>Modul<span class="text-danger">*</span></label>
                                    <select class="form-control" name="module_id" id="module_id">
                                    	<option value="">- Pilih Modul -</option>
                                    	@if(sizeof($modules) > 0) 
                                    		@foreach($modules as $module)
                                    			<option value="{{ $module->module_id }}">{{ $module->module_name }}</option>
                                    		@endforeach
                                    	@endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Task<span class="text-danger">*</span></label>
                                    <select class="form-control select2" name="task_name" id="task_name">
                                    	<!-- <option value="">- Pilih Task -</option> -->
                                    	<option value="index">Index</option>
                                    	<option value="create">Create</option>
                                    	<option value="edit">Edit</option>
                                    	<option value="delete">Delete</option>
                                    	<option value="view">View</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Add -->

<script type="text/javascript">
	$('.btnAdd').click(function(){
        $('#module_id').val('');
        $('#task_name').val('');
        $('.addModal form').attr('action', "{{ url('systask/store') }}");
        $('.addModal .modal-title').text('Tambah Task');
		$('.addModal').modal();
	});

    $('.btnEdit').click(function(){

        var id  = $(this).attr('data-id');
        var url = "{{ url('systask/getdata') }}";

        $('.addModal form').attr('action', "{{ url('systask/update') }}" +'/'+ id);

        $.ajax({
            type : 'GET',
            url : url +'/'+ id,
            dataType : 'JSON',
            success : function(data) {
                console.log(data);

                if (data.status == 1) {

                    $('#module_id').val(data.result.module_id);
                    $('#task_name').val(data.result.task_name);
                    $('.addModal .modal-title').text('Ubah Task');
                    $('.addModal').modal();

                }
                        
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                alert('Error : Gagal mengambil data'); 
            }
        });

    });

    $("#addForm").validate( {
        rules: {
            module_id: "required",
            task_name: "required",
        },
        messages: {
            module_id: "Modul harus dipilih",
            task_name: "Task harus dipilih",
        },
        errorElement: "em",
        errorClass: "invalid-feedback",
        errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            $(element).parents('.form-group').append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).addClass("is-invalid").removeClass("is-valid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).addClass("is-valid").removeClass("is-invalid");
        }
    });
</script>
@endsection