@extends('layouts.app_landing')
@section('title', 'Produk')
<?php use App\Helpers\DateFormatHelper; ?>

@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-10 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">GM-Tactical Warehouse</h4>
            <div class="d-flex align-items-center">
            </div>
        </div>
        <div class="col-2 align-self-center">
            <a href="/login" class="form-control btn btn-primary"><i class="fas fa-lock"></i> Login</a>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    <!-- basic table -->
    <div class="row">
        
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center mb-4">
                        <h4 class="card-title">Best Seller</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table no-wrap v-middle mb-0">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="55%">Produk</th>
                                    <th width="20%">Total Keluar</th>
                                    <th width="5%">Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (sizeof($best_seller_gk) == 0)
                                	<tr>
                                		<td colspan="3" align="center">Data kosong</td>
                                	</tr>
                                @else
                                	@foreach ($best_seller_gk as $row)
                                		<tr>
                                			<td width="5%">{{ $loop->iteration }}</td>
                                            <td width="40%">{{ $row->product_name }}</td>
                                			<td width="20%">
                                                {{ $row->total }}    
                                            </td>
                                			<td width="20%">
                                                <a href="{{ url('detail/'. $row->product_name) }}" class="btn btn-sm btn-outline-secondary">
                                                    <i class="fas fa-chart-line"></i>
                                                </a>   
                                            </td>
                                		</tr>
                                	@endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4">
                    	<div class="col-md-6">
                    		<h4 class="card-title">Cari Produk</h4>
                    	</div>
                    </div>
                    <form action="{{ url('/') }}" method="GET" id="addForm">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>Nama Produk <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="product_name" id="product_name" placeholder="Masukan nama produk">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Varian <span class="text-danger">*</span></label>
                                        <select class="form-control" name="product_variant">
                                            <option value="">- Pilih Semua -</option>
                                            @if(sizeof($product_variants) > 0)

                                                @foreach($product_variants as $pv)
                                                    <option value="{{ $pv->product_variant_id }}">{{ $pv->product_variant }}</option>
                                                @endforeach

                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Ukuran <span class="text-danger">*</span></label>
                                        <select class="form-control" name="product_size">
                                            <option value="">- Pilih Semua -</option>
                                            @if(sizeof($product_sizes) > 0)

                                                @foreach($product_sizes as $ps)
                                                    <option value="{{ $ps->product_size_id }}">{{ $ps->product_size }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>.</label>
                                        <button type="submit" class="form-control btn btn-primary"><i class="fas fa-search-plus"></i> Cari</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4">
                    	<div class="col-md-6">
                    		<h4 class="card-title">Daftar Produk</h4>
                    	</div>
                    </div>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th width="30%">Nama</th>
                                        <th width="25%">Varian</th>
                                        <th width="10%">Ukuran</th>
                                        <th width="15%">Gudang</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($show_products == null)
                                        <tr>
                                                <td colspan="6" align="center">Data kosong</td>
                                        </tr>
                                    @else
                                        @foreach ($show_products as $sp)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $sp->product_name }}</td>
                                                <td>{{ $sp->product_variant }}</td>
                                                <td>{{ $sp->product_size}}</td>
                                                <td width="20%">
                                                    @if($sp->stock_a <= 10)
                                                        <span class="badge badge-danger">{{ $sp->stock_a }}</span>
                                                    @else
                                                        {{ $sp->stock_a }}
                                                    @endif         
                                                </td>                                             
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<script>

$("#addForm").validate( {
    rules: {
        product_name: "required",
    },
    messages: {
        product_name: "nama produk tidak boleh kosong",
    },
    errorElement: "em",
    errorClass: "invalid-feedback",
    errorPlacement: function ( error, element ) {
        // Add the `help-block` class to the error element
        $(element).parents('.form-group').append(error);
    },
    highlight: function ( element, errorClass, validClass ) {
        $( element ).addClass("is-invalid").removeClass("is-valid");
    },
    unhighlight: function (element, errorClass, validClass) {
        $( element ).addClass("is-valid").removeClass("is-invalid");
    }
});
</script>
@endsection