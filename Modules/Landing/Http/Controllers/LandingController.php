<?php

namespace Modules\Landing\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Modules\Product\Repositories\ProductRepository;
use Modules\ProductVariant\Repositories\ProductVariantRepository;
use Modules\ProductSize\Repositories\ProductSizeRepository;
use Modules\Transaction\Repositories\TransactionRepository;

class LandingController extends Controller
{
    public function __construct()
    {
        $this->module = "Landing";

        $this->_productRepository = new ProductRepository;
        $this->_prodctSizeRepository = new ProductSizeRepository;
        $this->_prodctVariantRepository = new ProductVariantRepository;
        $this->_transactionRepository = new TransactionRepository;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {
        if (!empty($request->input('product_name'))) {
            $filter['product_name'] = $request->input('product_name');
        }

        if (!empty($request->input('product_variant'))) {
            $filter['product_variant_id'] = $request->input('product_variant');
        }

        if (!empty($request->input('product_size'))) {
            $filter['product_size_id'] = $request->input('product_size');
        }

        $products = $this->_productRepository->getAll();
        $product_sizes = $this->_prodctSizeRepository->getAll();
        $product_variants = $this->_prodctVariantRepository->getAll();

        if (!empty($filter)) {
            $show_products = $this->_productRepository->getAllByParams($filter);
        } else {
            $show_products = null;
        }

        $best_seller_gk = $this->_transactionRepository->getBestSellerByStat(0, 10);

        return view('landing::index', compact('products', 'product_sizes', 'product_variants', 'show_products', 'best_seller_gk'));

    }

    public function detail($name)
    {
        $product_stock = $this->_productRepository->stockProduct($name);

        return view('landing::detail', compact('product_stock'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('landing::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('landing::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('landing::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}