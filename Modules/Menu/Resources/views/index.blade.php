@extends('layouts.app')
@section('title', 'Menu')

@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Menu</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('') }}" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Menu</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    <!-- basic table -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4">
                    	<div class="col-md-6">
                    		<h4 class="card-title">Daftar Menu</h4>
                    	</div>
                    	<div class="col-md-6 text-right">
                    		<a href="javascript:void(0)" class="btn btn-primary btnAdd"><small class="fas fa-plus"></small> Tambah Menu</a>
                    	</div>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="20%">Label</th>
                                    <th width="15%">Parent</th>
                                    <th width="20%">URL</th>
                                    <th width="15%">Status</th>
                                    <th width="10%">Level</th>
                                    <th width="15%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (sizeof($menus) == 0)
                                	<tr>
                                		<td colspan="7" align="center">Data kosong</td>
                                	</tr>
                                @else
                                	@foreach ($menus as $menu)
                                		@php
                                			$parent = "-";

                                			if(!empty($menu->parent_id)) $parent = $menu->label_parent;

                                		@endphp

                                		<tr>
                                			<td width="5%">{{ $loop->iteration }}</td>
                                			<td width="20%">{{ $menu->label }}</td>
                                			<td width="20%">{{ $parent }}</td>
                                			<td width="20%">{{ $menu->url }}</td>
                                			<td width="15%">
                                                @if($menu->is_active == 1) 
                                                    <span class="badge badge-success">Aktif</span>
                                                @else
                                                    <span class="badge badge-danger">Tidak Aktif</span>
                                                @endif         
                                            </td>
                                			<td width="15%">{{ $menu->level }}</td>
                                			<td width="15%">
                                				<a href="javascript:void(0)" class="btn btn-sm btn-outline-secondary btnEdit" data-id="{{ $menu->menu_id }}">
                                					<i class="fas fa-pencil-alt"></i>
                                				</a>
                                				<a href="javascript:void(0)" class="btn btn-sm btn-outline-danger btnDelete" data-url="{{ url('menu/delete/'. $menu->menu_id) }}">
                                					<i class="fas fa-trash-alt"></i>
                                				</a>
                                			</td>
                                		</tr>
                                	@endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<!-- Modal Add -->
<div class="modal addModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ url('menu/store') }}" method="POST" id="addForm">
                @csrf
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                        	<div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="label" id="label" placeholder="Masukan label">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Level <span class="text-danger">*</span></label>
                                    <select name="level" id="level" class="form-control">
                                    	<option value="1">1</option>
                                        <option value="2">2</option>
                                    	<option value="3">3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>URL <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="url" id="url" placeholder="Masukan alamat URL">
                                    <small>Untuk menu parent diisi: #</small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Posisi <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="position" id="position" placeholder="Masukan posisi">
                                    <small>Semakin kecil, semakin awal (0)</small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Menu Parent</label>
                                    <select class="form-control" name="parent_id" id="parent_id" disabled="disabled">
                                    	<option value="">- Pilih Menu -</option>
                                    	@if(sizeof($parents) > 0) 
                                    		@foreach($parents as $parent)
                                    			<option value="{{ $parent->menu_id }}">{{ $parent->label }}</option>
                                    		@endforeach
                                    	@endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Status <span class="text-danger">*</span></label> <br>
                                    <label>
                                        <input type="radio" name="is_active" id="is_active_1" value="1" checked="checked"> Aktif
                                    </label>
                                    <label>
                                        <input type="radio" name="is_active" id="is_active_0" value="0"> Tidak Aktif
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Link External <span class="text-danger">*</span></label> <br>
                                    <label>
                                        <input type="radio" name="is_external_link" id="is_external_link_1" value="1" checked="checked"> Ya
                                    </label>
                                    <label>
                                        <input type="radio" name="is_external_link" id="is_external_link_0" value="0"> Tidak
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Add -->

<script type="text/javascript">
	$('.btnAdd').click(function(){
        $('#label').val('');
        $('#url').val('');
        $('#level').val('1');
        $('#position').val('');
        $('#parent_id').val('');
        $('#parent_id').attr('disabled', 'disabled');
        $('#is_active_0').removeAttr('checked');
        $('#is_active_1').attr('checked', 'checked');
        $('.addModal form').attr('action', "{{ url('menu/store') }}");
        $('.addModal .modal-title').text('Tambah Menu');
		$('.addModal').modal();
	});

    $('.btnEdit').click(function(){

        var id  = $(this).attr('data-id');
        var url = "{{ url('menu/getdata') }}";

        $('.addModal form').attr('action', "{{ url('menu/update') }}" +'/'+ id);

        $.ajax({
            type : 'GET',
            url : url +'/'+ id,
            dataType : 'JSON',
            success : function(data) {
                console.log(data);

                if (data.status == 1) {

                    $('#label').val(data.result.label);
			        $('#url').val(data.result.url);
                    $('#level').val(data.result.level);
			        $('#position').val(data.result.position);
			        $('#parent_id').val(data.result.parent_id);

			        if (data.result.level == 1) {
			        	$('#parent_id').attr('disabled', 'disabled');
			        }
			        else{
			        	$('#parent_id').removeAttr('disabled');
			        }

			        if (data.result.is_active == 1) {
			        	$('#is_active_0').removeAttr('checked');
        				$('#is_active_1').attr('checked', 'checked');
			        }
			        else{
			        	$('#is_active_1').removeAttr('checked');
        				$('#is_active_0').attr('checked', 'checked');
			        }

			        if (data.result.is_external_link == 1) {
                        $('#is_external_link_0').removeAttr('checked');
        				$('#is_external_link_1').attr('checked', 'checked');
			        }
			        else{
                        $('#is_external_link_1').removeAttr('checked');
        				$('#is_external_link_0').attr('checked', 'checked');
			        }

                    $('.addModal .modal-title').text('Ubah Menu');
                    $('.addModal').modal();

                }
                        
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                alert('Error : Gagal mengambil data'); 
            }
        });

    });

    $('#level').change(function(){

        var val = $(this).val();

        if (val == 1) {
            $('#parent_id').val('');
            $('#parent_id').attr('disabled', 'disabled');
        }
        else{
            $('#parent_id').removeAttr('disabled');
        }

    });

    $("#addForm").validate( {
        rules: {
            label: "required",
            url: "required",
            level: "required",
            position: {
                required: true,
                number: true
            }
        },
        messages: {
            label: "Label tidak boleh kosong",
            url: "URL tidak boleh kosong",
            level: "Level harus dipilih",
            position: {
                required: "Posisi tidak boleh kosong",
                number: "Hanya boleh berisi angka"
            }
        },
        errorElement: "em",
        errorClass: "invalid-feedback",
        errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            $(element).parents('.form-group').append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).addClass("is-invalid").removeClass("is-valid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).addClass("is-valid").removeClass("is-invalid");
        }
    });
</script>
@endsection