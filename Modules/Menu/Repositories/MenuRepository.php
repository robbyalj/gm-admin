<?php

namespace Modules\Menu\Repositories;

use App\Implementations\QueryBuilderImplementation;
use Illuminate\Support\Facades\DB;

class MenuRepository extends QueryBuilderImplementation
{

    public $fillable = ['label', 'parent_id', 'level', 'position', 'url', 'is_active', 'is_external_link', 'created_at', 'created_by', 'updated_at', 'updated_by'];

    public function __construct()
    {
        $this->table = 'menus';
        $this->pk = 'menu_id';
    }

    public function getAll()
    {
        try {
            return DB::connection($this->db)
                ->table($this->table)
                ->select('menus.*', 'parent.label as label_parent')
                ->leftJoin('menus as parent', 'parent.menu_id', '=', 'menus.parent_id')
                ->get();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    // overide
    public function getAllByParams(array $params)
    {
        try {
            return DB::connection($this->db)
                ->table($this->table)
                ->where('level', '1')
                ->orWhere('level', '2')
                ->get();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

}