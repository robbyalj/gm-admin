<?php

namespace Modules\Setting\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

use Modules\Setting\Repositories\SettingRepository;
use App\Helpers\DataHelper;
use App\Helpers\LogHelper;
use DB;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->module = "Setting";

        $this->_settingRepository = new SettingRepository;
        $this->_logHelper = new LogHelper;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $settings = $this->_settingRepository->getAll();

        return view('setting::index', compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('setting::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $validated = $request->validate(
            [
                'setting_name' => 'required|unique:settings,setting_name',
            ],
            [
                'setting_name.unique' => 'setting sudah terinput',
            ]
        );

        DB::beginTransaction();
        $this->_settingRepository->insert(DataHelper::_normalizeParams($request->all(), true));

        $this->_logHelper->store($this->module, $request->setting_name, 'create');
        DB::commit();
        return redirect('setting')->with('message', 'Setting telah berhasil ditambahkan');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('setting::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('setting::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $validated = $request->validate(
            [
                'setting_name' => 'required|unique:settings,setting_name,' . $id . ',setting_id',
            ],
            [
                'setting_name.unique' => 'setting sudah terinput',
            ]
        );

        // Check detail to db
        $detail = $this->_settingRepository->getById($id);

        if (!$detail) {
            return redirect('setting');
        }
        DB::beginTransaction();

        $this->_settingRepository->update(DataHelper::_normalizeParams($request->all(), false, true), $id);
        $this->_logHelper->store($this->module, $request->setting_name, 'update');

        DB::commit();

        return redirect('setting')->with('message', 'Setting berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        // Check detail to db
        $detail = $this->_settingRepository->getById($id);

        if (!$detail) {
            return redirect('merchant');
        }

        DB::beginTransaction();

        $this->_settingRepository->delete($id);
        $this->_logHelper->store($this->module, $detail->setting_name, 'delete');

        DB::commit();

        return redirect('setting')->with('message', 'Setting berhasil dihapus');
    }

    /**
     * Get data the specified resource in storage.
     * @param int $id
     * @return Response
     */
    public function getdata($id)
    {

        $response = array('status' => 0, 'result' => array());
        $getDetail = $this->_settingRepository->getById($id);

        if ($getDetail) {
            $response['status'] = 1;
            $response['result'] = $getDetail;
        }

        return $response;
    }
}