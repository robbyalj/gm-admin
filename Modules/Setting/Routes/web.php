<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('setting')->group(function () {
    Route::get('/', 'SettingController@index');
    Route::get('/create', 'SettingController@create');
    Route::get('/show/{id}', 'SettingController@show');
    Route::get('/edit/{id}', 'SettingController@edit');
    Route::post('/store', 'SettingController@store');
    Route::post('/update/{id}', 'SettingController@update');
    Route::get('/delete/{id}', 'SettingController@destroy');
    Route::get('/getdata/{id}', 'SettingController@getdata');
});