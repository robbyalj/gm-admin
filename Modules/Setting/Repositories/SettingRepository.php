<?php

namespace Modules\Setting\Repositories;

use App\Implementations\QueryBuilderImplementation;
use DB;

class SettingRepository extends QueryBuilderImplementation
{

    public $fillable = ['setting_name', 'setting_value', 'created_by', 'created_at', 'updated_by', 'updated_at'];

    public function __construct()
    {
        $this->table = 'settings';
        $this->pk = 'setting_id';
    }

    public function getValue($name)
    {
        try {
            $query = DB::table($this->table)
                ->select('setting_value')
                ->where('setting_name', '=', $name)
                ->first();

        } catch (Exception $e) {
            die($e->getMessage());
        }
        return $query;
    }

}