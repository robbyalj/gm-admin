<?php

namespace Modules\HistoryProductIn\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

use Modules\Product\Repositories\ProductRepository;
use Modules\ProductVariant\Repositories\ProductVariantRepository;
use Modules\ProductSize\Repositories\ProductSizeRepository;
use Modules\Transaction\Repositories\TransactionRepository;
use App\Helpers\LogHelper;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\HistoryProductInExport;

class HistoryProductInController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->module = "HistoryProductIn";

        $this->_productRepository = new ProductRepository;
        $this->_prodctSizeRepository = new ProductSizeRepository;
        $this->_prodctVariantRepository = new ProductVariantRepository;
        $this->_transactionRepository = new TransactionRepository;

        $this->_logHelper = new LogHelper;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index(Request $request)
    {

        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        if (!empty($request->input('product_name'))) {
            $filter['product_name'] = $request->input('product_name');
        }

        if (!empty($request->input('product_variant'))) {
            $filter['product_variant_id'] = $request->input('product_variant');
        }

        if (!empty($request->input('product_size'))) {
            $filter['product_size_id'] = $request->input('product_size');
        }

        if (!empty($request->input('start_date'))) {
            $filter['start_date'] = $request->input('start_date');
        }

        if (!empty($request->input('finish_date'))) {
            $filter['finish_date'] = $request->input('finish_date');
        }

        if (!empty($request->input('gudang'))) {
            $mark = $request->input('gudang');
        } else {
            $mark = null;
        }

        $products = $this->_productRepository->getAll();
        $product_sizes = $this->_prodctSizeRepository->getAll();
        $product_variants = $this->_prodctVariantRepository->getAll();

        if (!empty($filter)) {
            $filter['status'] = '1';
            $show_products = $this->_transactionRepository->getAllByParams($filter);
        } else {
            $show_products = null;
        }

        return view('historyproductin::index', compact('products', 'product_sizes', 'product_variants', 'show_products', 'mark'));
    }

    public function excel(Request $request)
    {
        $filter = array();

        if (!empty($request->input('product_name'))) {
            $filter['product_name'] = $request->input('product_name');
        }

        if (!empty($request->input('product_variant'))) {
            $filter['product_variant_id'] = $request->input('product_variant');
        }

        if (!empty($request->input('product_size'))) {
            $filter['product_size_id'] = $request->input('product_size');
        }

        if (!empty($request->input('start_date'))) {
            $filter['start_date'] = $request->input('start_date');
        }

        if (!empty($request->input('finish_date'))) {
            $filter['finish_date'] = $request->input('finish_date');
        }

        if (!empty($request->input('gudang'))) {
            $mark = $request->input('gudang');
        } else {
            $mark = null;
        }

        return Excel::download(new HistoryProductInExport($filter), 'BarangMasuk_' . md5(time() . uniqid()) . '.xls');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('historyproductin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('historyproductin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('historyproductin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}