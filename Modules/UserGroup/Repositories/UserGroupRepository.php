<?php

namespace Modules\UserGroup\Repositories;

use App\Implementations\QueryBuilderImplementation;

class UserGroupRepository extends QueryBuilderImplementation
{

	public $fillable = ['group_name'];

    public function __construct()
    {
        $this->table = 'user_groups';
        $this->pk = 'group_id';
    }

}