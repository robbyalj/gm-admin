@extends('layouts.app')
@section('title', 'Kategori Berita')

@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Ukuran Produk</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('') }}" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item"><a href="#" class="text-muted">Master</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Ukuran Produk</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @if(session()->get('errors'))
        <div class="alert alert-danger">
            {{ session()->get('errors')->first() }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
    @endif
    <!-- basic table -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4">
                    	<div class="col-md-6">
                    		<h4 class="card-title">Daftar Ukuran Produk</h4>
                    	</div>
                    	<div class="col-md-6 text-right">
                    		<a href="javascript:void(0)" class="btn btn-primary btnAdd"><small class="fas fa-plus"></small> Tambah Ukuran Produk</a>
                    	</div>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered no-wrap">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="20%">Nama</th>
                                    <th width="60%">Deskripsi</th>
                                    <th width="15%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (sizeof($names) == 0)
                                	<tr>
                                		<td colspan="4" align="center">Data kosong</td>
                                	</tr>
                                @else
                                	@foreach ($names as $name)
                                		<tr>
                                			<td width="5%">{{ $loop->iteration }}</td>
                                			<td width="80%">{{ $name->product_size }}</td>
                                			<td width="80%">{{ $name->description }}</td>
                                			<td width="15%">
                                				<a href="javascript:void(0)" class="btn btn-sm btn-outline-secondary btnEdit" data-id="{{ $name->product_size_id }}">
                                					<i class="fas fa-pencil-alt"></i>
                                				</a>
                                				<a href="javascript:void(0)" class="btn btn-sm btn-outline-danger btnDelete" data-url="{{ url('productsize/delete/'. $name->product_size_id) }}">
                                					<i class="fas fa-trash-alt"></i>
                                				</a>
                                			</td>
                                		</tr>
                                	@endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<!-- Modal Add -->
<div class="modal addModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Ukuran Produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ url('productsize/store') }}" method="POST" id="addForm">
                @csrf
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Ukuran Produk <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="product_size" id="product_size" placeholder="Masukan ukuran produk">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Deskripsi <span class="text-danger"></span></label>
                                    <textarea class="form-control" name="description" id="description" placeholder="Masukan deskripsi"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal Add -->

<script type="text/javascript">
	$('.btnAdd').click(function(){
        $('#product_size').val('');
        $('#description').val('');
        $('.addModal form').attr('action', "{{ url('productsize/store') }}");
        $('.addModal .modal-title').text('Tambah Ukuran Produk');
		$('.addModal').modal();
	});

    $('.btnEdit').click(function(){

        var id  = $(this).attr('data-id');
        var url = "{{ url('productsize/getdata') }}";

        $('.addModal form').attr('action', "{{ url('productsize/update') }}" +'/'+ id);

        $.ajax({
            type : 'GET',
            url : url +'/'+ id,
            dataType : 'JSON',
            success : function(data) {
                console.log(data);

                if (data.status == 1) {

                    $('#product_size').val(data.result.product_size);
                    $('#description').val(data.result.description);
                    $('.addModal .modal-title').text('Ubah Ukuran Produk');
                    $('.addModal').modal();

                }
                        
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                alert('Error : Gagal mengambil data'); 
            }
        });

    });

    $("#addForm").validate( {
        rules: {
            product_size: "required",
            description: "required",
        },
        messages: {
            product_size: "Ukuran produk tidak boleh kosong",
            description: "Deskripsi tidak boleh kosong",
        },
        errorElement: "em",
        errorClass: "invalid-feedback",
        errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            $(element).parents('.form-group').append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).addClass("is-invalid").removeClass("is-valid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).addClass("is-valid").removeClass("is-invalid");
        }
    });
</script>
@endsection