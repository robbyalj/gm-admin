<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('productsize')->group(function() {
    Route::get('/', 'ProductSizeController@index');
    Route::get('/', 'ProductSizeController@index');\
    Route::get('/create', 'ProductSizeController@create');
    Route::get('/show/{id}', 'ProductSizeController@show');
    Route::get('/edit/{id}', 'ProductSizeController@edit');
    Route::post('/store', 'ProductSizeController@store');
    Route::post('/update/{id}', 'ProductSizeController@update');
    Route::get('/delete/{id}', 'ProductSizeController@destroy');
    Route::get('/getdata/{id}', 'ProductSizeController@getdata');
});
