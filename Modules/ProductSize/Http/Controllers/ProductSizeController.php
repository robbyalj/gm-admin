<?php

namespace Modules\ProductSize\Http\Controllers;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

use Modules\ProductSize\Repositories\ProductSizeRepository;
use App\Helpers\DataHelper;
use App\Helpers\LogHelper;
use DB;


class ProductSizeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->module = "ProductSize";

        $this->_productSizeRepository = new ProductSizeRepository;
        $this->_logHelper             = new LogHelper;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
         // Authorize
         if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $names = $this->_productSizeRepository->getAll();
        
        return view('productsize::index', compact('names'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }
        return view('productsize::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
       // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $validated = $request->validate([
            'product_size' => 'required|unique:product_sizes,product_size',
        ],
        [
            'product_size.unique' => 'size sudah terinput',
        ]);

        DB::beginTransaction(); 
        $this->_productSizeRepository->insert(DataHelper::_normalizeParams($request->all(), true));

        $this->_logHelper->store($this->module, $request->product_size, 'create');
        DB::commit();
        return redirect('productsize')->with('message', 'Ukuran Produk telah berhasil ditambahkan');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        return view('productsize::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }
        return view('productsize::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $validated = $request->validate([
            'product_size' => 'required|unique:product_sizes,product_size,'.$id.',product_size_id',
        ],
        [
            'product_size.unique' => 'size sudah terinput',
        ]);

        // Check detail to db
        $detail  = $this->_productSizeRepository->getById($id);

        if (!$detail) {
            return redirect('productsize');
        }
        DB::beginTransaction();
        
        $this->_productSizeRepository->update(DataHelper::_normalizeParams($request->all(), false, true), $id);
        $this->_logHelper->store($this->module, $request->product_size, 'update');

        DB::commit();

        return redirect('productsize')->with('message', 'Ukuran Produk berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }
        
        // Check detail to db
        $detail  = $this->_productSizeRepository->getById($id);

        if (!$detail) {
            return redirect('productsize');
        }

        DB::beginTransaction();

        $this->_productSizeRepository->delete($id);
        $this->_logHelper->store($this->module, $detail->product_size, 'delete');

        DB::commit();

        return redirect('productsize')->with('message', 'Ukuran Produk berhasil dihapus');
    }
    public function getdata($id){

        $response   = array('status' => 0, 'result' => array()); 
        $getDetail  = $this->_productSizeRepository->getById($id);

        if ($getDetail) {
            $response['status'] = 1;
            $response['result'] = $getDetail;
        }

        return $response;

    }
}
