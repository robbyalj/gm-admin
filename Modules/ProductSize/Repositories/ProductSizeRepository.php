<?php

namespace Modules\ProductSize\Repositories;

use App\Implementations\QueryBuilderImplementation;
use DB;

class ProductSizeRepository extends QueryBuilderImplementation
{

	public $fillable = ['product_size', 'description', 'created_by', 'created_at', 'updated_by', 'updated_at'];

    public function __construct()
    {
        $this->table = 'product_sizes';
        $this->pk = 'product_size_id';
    }

    //override
    public function getAll()
    {
        try {
            return DB::connection($this->db)
                ->table($this->table)
                // ->orderBy(DB::raw('ABS(product_size)'), 'ASC')
                ->orderBy('product_size', 'ASC')
                // ->orderByRaw('product_size * 1 ASC')
                ->get();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }


}