@extends('layouts.app')
@section('title', 'Transaksi Produk Masuk')
<?php use App\Helpers\DateFormatHelper; ?>

@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Transaksi Produk Keluar</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('') }}" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Transaksi Produk Keluar</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    <!-- basic table -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4">
                    	<div class="col-md-6">
                    		<h4 class="card-title">Cari Transaksi</h4>
                    	</div>
                    </div>
                    <form action="{{ url('historyproductout') }}" method="GET" id="addForm">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Nama Produk <span class="text-danger"></span></label>
                                        <input type="text" class="form-control" name="product_name" id="product_name" placeholder="Masukan nama produk">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Varian <span class="text-danger"></span></label>
                                        <select class="form-control" name="product_variant">
                                            <option value="">- Pilih Semua -</option>
                                            @if(sizeof($product_variants) > 0)

                                                @foreach($product_variants as $pv)
                                                    <option value="{{ $pv->product_variant_id }}">{{ $pv->product_variant }}</option>
                                                @endforeach

                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Ukuran <span class="text-danger"></span></label>
                                        <select class="form-control" name="product_size">
                                            <option value="">- Pilih Semua -</option>
                                            @if(sizeof($product_sizes) > 0)

                                                @foreach($product_sizes as $ps)
                                                    <option value="{{ $ps->product_size_id }}">{{ $ps->product_size }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Dari Tanggal <span class="text-danger">*</span></label>
                                        <input type="date" class="form-control" name="start_date" id="start_date" placeholder="Masukan nama produk">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Sampai Tanggal <span class="text-danger">*</span></label>
                                        <input type="date" class="form-control" name="finish_date" id="finish_date" placeholder="Masukan nama produk">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Gudang <span class="text-danger"></span></label>
                                        <select class="form-control" name="gudang">
                                            <option value="1">- Gudang Besar -</option>
                                            <!--<option value="1">- Gudang Besar -</option>-->
                                            <!--<option value="2">- Gudang Kecil -</option>-->
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>.</label>
                                        <button type="submit" class="form-control btn btn-primary"><i class="fas fa-search-plus"></i> Cari</button>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>.</label>
                                        <button type="submit" formaction="{{ url('historyproductout/download-excel') }}" class="form-control btn btn-success"><i class="fas fa-file-excel"></i>  Download Excel </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4">
                    	<div class="col-md-6">
                    		<h4 class="card-title">Daftar Transaksi</h4>
                    	</div>
                    </div>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered no-wrap">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th >Varian</th>
                                        <th>Ukuran</th>
                                        @if ($mark == null)
                                            <th>GB Jumlah Input</th>
                                            <th>GB Jumlah Akhir</th>
                                            <th>GK Jumlah Input</th>
                                            <th>GK Jumlah Akhir</th>
                                        @else
                                            @if ($mark == 1)
                                                <th>GB Jumlah Input</th>
                                                <th>GB Jumlah Akhir</th>
                                            @else
                                                <th>GK Jumlah Input</th>
                                                <th>GK Jumlah Akhir</th>
                                            @endif
                                        @endif
                                        <th>Waktu</th>
                                        <th>User</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if ($show_products == null)
                                        <tr>
                                            @if ($mark == null)
                                                <td colspan="10" align="center">Data kosong</td>
                                            @else
                                                <td colspan="8" align="center">Data kosong</td>
                                            @endif
                                        </tr>
                                    @else
                                        @foreach ($show_products as $sp)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $sp->product_name }}</td>
                                                <td>{{ $sp->product_variant }}</td>
                                                <td>{{ $sp->product_size}}</td>
                                                @if ($mark == null)
                                                    <td>{{ $sp->input_stock_a}}</td>
                                                    <td>{{ $sp->current_stock_a}}</td>
                                                    <td>{{ $sp->input_stock_b}}</td>
                                                    <td>{{ $sp->current_stock_b}}</td>
                                                @else
                                                    @if ($mark == 1)
                                                        <td>{{ $sp->input_stock_a}}</td>
                                                        <td>{{ $sp->current_stock_a}}</td>
                                                    @else
                                                        <td>{{ $sp->input_stock_b}}</td>
                                                        <td>{{ $sp->current_stock_b}}</td>
                                                    @endif
                                                @endif
                                                <td>{{ $sp->created_at }}</td>
                                                <td>{{ $sp->name}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<script>

$("#addForm").validate( {
    rules: {
        start_date: "required",
        finish_date: "required",
        gudang: "required",
    },
    messages: {
        start_date: "tangaal mulai tidak boleh kosong",
        finish_date: "tanggal akhir tidak boleh kosong",
        gudang: "gudang akhir tidak boleh kosong",
    },
    errorElement: "em",
    errorClass: "invalid-feedback",
    errorPlacement: function ( error, element ) {
        // Add the `help-block` class to the error element
        $(element).parents('.form-group').append(error);
    },
    highlight: function ( element, errorClass, validClass ) {
        $( element ).addClass("is-invalid").removeClass("is-valid");
    },
    unhighlight: function (element, errorClass, validClass) {
        $( element ).addClass("is-valid").removeClass("is-invalid");
    }
});

</script>
@endsection