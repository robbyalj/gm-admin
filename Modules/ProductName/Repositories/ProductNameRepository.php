<?php

namespace Modules\ProductName\Repositories;

use App\Implementations\QueryBuilderImplementation;
use DB;

class ProductNameRepository extends QueryBuilderImplementation
{

	public $fillable = ['product_name', 'description', 'created_by', 'created_at', 'updated_by', 'updated_at'];

    public function __construct()
    {
        $this->table = 'product_names';
        $this->pk = 'product_name_id';
    }

     //override
     public function getAll()
     {
         try {
             return DB::connection($this->db)
                 ->table($this->table)
                 ->orderBy('product_name', 'ASC')
                 ->get();
         } catch (Exception $e) {
             return $e->getMessage();
         }
     }
 

}