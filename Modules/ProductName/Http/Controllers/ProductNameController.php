<?php

namespace Modules\ProductName\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

use Modules\ProductName\Repositories\ProductNameRepository;
use App\Helpers\DataHelper;
use App\Helpers\LogHelper;
use DB;

class ProductNameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->module = "ProductName";

        $this->_productNameRepository = new ProductNameRepository;
        $this->_logHelper             = new LogHelper;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
         // Authorize
         if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $names = $this->_productNameRepository->getAll();
        
        return view('productname::index', compact('names'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }
        return view('productname::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $validated = $request->validate([
            'product_name' => 'required|unique:product_names,product_name',
        ],
        [
            'product_name.unique' => 'nama produk sudah terinput',
        ]);

        DB::beginTransaction(); 
        $this->_productNameRepository->insert(DataHelper::_normalizeParams($request->all(), true));

        $this->_logHelper->store($this->module, $request->product_name, 'create');
        DB::commit();
        return redirect('productname')->with('message', 'Nama produk telah berhasil ditambahkan');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
         // Authorize
         if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        return view('productname::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }
        return view('productname::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $validated = $request->validate([
            'product_name' => 'required|unique:product_names,product_name,'.$id.',product_name_id',
        ],
        [
            'product_name.unique' => 'nama produk sudah terinput',
        ]);

        // Check detail to db
        $detail  = $this->_productNameRepository->getById($id);

        if (!$detail) {
            return redirect('productname');
        }
        DB::beginTransaction();
        
        $this->_productNameRepository->update(DataHelper::_normalizeParams($request->all(), false, true), $id);
        $this->_logHelper->store($this->module, $request->product_name, 'update');

        DB::commit();

        return redirect('productname')->with('message', 'Nama produk berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }
        
        // Check detail to db
        $detail  = $this->_productNameRepository->getById($id);

        if (!$detail) {
            return redirect('productname');
        }

        DB::beginTransaction();

        $this->_productNameRepository->delete($id);
        $this->_logHelper->store($this->module, $detail->product_name, 'delete');

        DB::commit();

        return redirect('productname')->with('message', 'Nama Produk berhasil dihapus');
    }

    /**
     * Get data the specified resource in storage.
     * @param int $id
     * @return Response
     */
    public function getdata($id){

        $response   = array('status' => 0, 'result' => array()); 
        $getDetail  = $this->_productNameRepository->getById($id);

        if ($getDetail) {
            $response['status'] = 1;
            $response['result'] = $getDetail;
        }

        return $response;
    }
}
