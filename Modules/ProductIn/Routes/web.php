<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('productin')->group(function() {
    Route::get('/', 'ProductInController@index');
    Route::get('/create', 'ProductInController@create');
    Route::get('/show/{id}', 'ProductInController@show');
    Route::get('/edit/{id}', 'ProductInController@edit');
    Route::post('/store', 'ProductInController@store');
    Route::post('/update/{id}', 'ProductInController@update');
    Route::get('/delete/{id}', 'ProductInController@destroy');
    Route::get('/getdata/{id}', 'ProductInController@getdata');
    Route::post('/print', 'ProductInController@print');
});
