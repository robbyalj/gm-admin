<?php

namespace Modules\LogActivity\Repositories;

use App\Implementations\QueryBuilderImplementation;

class LogActivityRepository extends QueryBuilderImplementation
{

	public $fillable = ['log_activity_desc', 'created_at'];

    public function __construct()
    {
        $this->table = 'log_activities';
        $this->pk = 'log_activity_id';
    }

}