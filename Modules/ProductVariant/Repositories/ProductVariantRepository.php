<?php

namespace Modules\ProductVariant\Repositories;

use App\Implementations\QueryBuilderImplementation;
use DB;

class ProductVariantRepository extends QueryBuilderImplementation
{

	public $fillable = ['product_variant', 'description', 'created_by', 'created_at', 'updated_by', 'updated_at'];

    // protected $primaryKey = 'product_variant_id';

    public function __construct()
    {
        $this->table = 'product_variants';
        $this->pk = 'product_variant_id';
    }

    //override
    public function getAll()
    {
        try {
            return DB::connection($this->db)
                ->table($this->table)
                ->orderBy('product_variant', 'ASC')
                ->get();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

}