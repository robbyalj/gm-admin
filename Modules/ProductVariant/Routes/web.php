<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('productvariant')->group(function() {
    Route::get('/', 'ProductVariantController@index');
    Route::get('/', 'ProductVariantController@index');
    Route::get('/create', 'ProductVariantController@create');
    Route::get('/show/{id}', 'ProductVariantController@show');
    Route::get('/edit/{id}', 'ProductVariantController@edit');
    Route::post('/store', 'ProductVariantController@store');
    Route::post('/update/{id}', 'ProductVariantController@update');
    Route::get('/delete/{id}', 'ProductVariantController@destroy');
    Route::get('/getdata/{id}', 'ProductVariantController@getdata');
});
