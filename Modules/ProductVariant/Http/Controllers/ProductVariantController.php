<?php

namespace Modules\ProductVariant\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;

use Modules\ProductVariant\Repositories\ProductVariantRepository;
use App\Helpers\DataHelper;
use App\Helpers\LogHelper;
use DB;

class ProductVariantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->module = "ProductVariant";

        $this->_productVariantRepository = new ProductVariantRepository;
        $this->_logHelper             = new LogHelper;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $names = $this->_productVariantRepository->getAll();
        
        return view('productvariant::index', compact('names'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }
        return view('productvariant::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $validated = $request->validate([
            'product_variant' => 'required|unique:product_variants,product_variant',
        ],
        [
            'product_variant.unique' => 'varian sudah terinput',
        ]);

        DB::beginTransaction(); 
        $this->_productVariantRepository->insert(DataHelper::_normalizeParams($request->all(), true));

        $this->_logHelper->store($this->module, $request->product_variant, 'create');
        DB::commit();
        return redirect('productvariant')->with('message', 'Varian telah berhasil ditambahkan');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        return view('productvariant::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }
        return view('productvariant::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        // Authorize
        if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }

        $validated = $request->validate([
            'product_variant' => 'required|unique:product_variants,product_variant,'.$id.',product_variant_id',
        ],
        [
            'product_variant.unique' => 'varian sudah terinput',
        ]);

        // Check detail to db
        $detail  = $this->_productVariantRepository->getById($id);

        if (!$detail) {
            return redirect('productvariant');
        }
        DB::beginTransaction();
        
        $this->_productVariantRepository->update(DataHelper::_normalizeParams($request->all(), false, true), $id);
        $this->_logHelper->store($this->module, $request->product_variant, 'update');

        DB::commit();

        return redirect('productvariant')->with('message', 'Varian berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
         // Authorize
         if (Gate::denies(__FUNCTION__, $this->module)) {
            return redirect('unauthorize');
        }
        
        // Check detail to db
        $detail  = $this->_productVariantRepository->getById($id);

        if (!$detail) {
            return redirect('productvariant');
        }

        DB::beginTransaction();

        $this->_productVariantRepository->delete($id);
        $this->_logHelper->store($this->module, $detail->product_variant, 'delete');

        DB::commit();

        return redirect('productvariant')->with('message', 'Varian berhasil dihapus');
    }
    public function getdata($id){

        $response   = array('status' => 0, 'result' => array()); 
        $getDetail  = $this->_productVariantRepository->getById($id);

        if ($getDetail) {
            $response['status'] = 1;
            $response['result'] = $getDetail;
        }

        return $response;

    }
}
