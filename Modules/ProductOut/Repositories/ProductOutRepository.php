<?php

namespace Modules\ProductOut\Repositories;

use App\Implementations\QueryBuilderImplementation;

class ProductOutRepository extends QueryBuilderImplementation
{

	public $fillable = ['product_variant', 'description', 'created_by', 'created_at', 'updated_by', 'updated_at'];

    public function __construct()
    {
        $this->table = 'product_variants';
        $this->pk = 'product_variant_id';
    }

}