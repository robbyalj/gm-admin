<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('productout')->group(function() {
    Route::get('/', 'ProductOutController@index');
    Route::get('/create', 'ProductOutController@create');
    Route::get('/show/{id}', 'ProductOutController@show');
    Route::get('/edit/{id}', 'ProductOutController@edit');
    Route::post('/store', 'ProductOutController@store');
    Route::post('/update/{id}', 'ProductOutController@update');
    Route::get('/delete/{id}', 'ProductOutController@destroy');
    Route::get('/getdata/{id}', 'ProductOutController@getdata');
    Route::post('/print', 'ProductOutController@print');
});
