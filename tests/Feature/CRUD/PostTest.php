<?php

namespace Tests\Feature\CRUD;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Repositories\PostRepository;
use App\Repositories\UserRepository;

class PostTest extends TestCase
{

    use RefreshDatabase;

    private $_postRepository;
    private $_userRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->_postRepository = new PostRepository;
        $this->_userRepository = new UserRepository;
    }

    /** @test */
    public function can_view_index_page()
    {
        $this->withoutExceptionHandling();

        $response = $this->get('/posts');

        $response->assertStatus(200);
    }

    /** @test */
    public function post_repository_can_be_loaded()
    {
        $this->withoutExceptionHandling();

        $this->assertCount(0, $this->_postRepository->getAll());
    }

    /** @test */
    public function can_view_get_all_post()
    {
        $this->withoutExceptionHandling();

        $response = $this->get('/posts');

        $response->assertViewHas('posts');
    }

    /** @test */
    public function post_can_be_created()
    {
        $this->withoutExceptionHandling();

        $this->_userRepository->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => '',
            'created_at' => date('Y-m-d H:i:s'),
            'is_active' => 1,
        ]);

        $user = $this->_userRepository->getFirst();

        $response = $this->post('/posts', [
            'title' => 'title #1',
            'content' => 'content #1',
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $user->user_id,
        ]);

        $post = $this->_postRepository->getFirst();

        $this->assertEquals('title #1', $post->title);
        $this->assertEquals('content #1', $post->content);
    }

    /** @test */
    public function post_can_be_updated()
    {
        $this->withoutExceptionHandling();

        $user = $this->_insertAndGetUser();
        $post = $this->_insertAndGetPost($user->user_id);

        $this->patch('/posts/' . $post->post_id . '/edit', [
            'title' => 'new title',
            'content' => 'new content',
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => $user->user_id,
        ]);

        $updatedPost = $this->_postRepository->getFirst();

        $this->assertEquals('new title', $updatedPost->title);
        $this->assertEquals('new content', $updatedPost->content);
    }

    /** @test */
    public function post_can_be_deleted()
    {
        $this->withoutExceptionHandling();

        $user = $this->_insertAndGetUser();
        $post = $this->_insertAndGetPost($user->user_id);

        $this->delete('/posts/' . $post->post_id . '/delete');

        $this->assertCount(0, $this->_postRepository->getAll());
    }

    private function _insertAndGetPost($createdBy)
    {
        $response = $this->post('/posts', [
            'title' => 'title #1',
            'content' => 'content #1',
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $createdBy,
        ]);

        return $this->_postRepository->getFirst();
    }

    private function _insertAndGetUser()
    {
        $this->_userRepository->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => '',
            'created_at' => date('Y-m-d H:i:s'),
            'is_active' => 1,
        ]);

        return $this->_userRepository->getFirst();
    }

}
