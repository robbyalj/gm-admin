FROM php:7.4-apache


# Install system dependencies
RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    libzip-dev\
    zip \
    unzip


# Install PHP extensions
RUN docker-php-ext-install pdo pdo_mysql mbstring exif pcntl gd zip 
RUN docker-php-ext-configure pcntl --enable-pcntl && docker-php-ext-install pcntl
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
# RUN php composer-setup.php --install-dir=bin --filename=composer

# RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
# RUN php -r "if (hash_file('sha384', 'composer-setup.php') === 'e21205b207c3ff031906575712edab6f13eb0b361f2085f1f1237b7126d785e826a450292b6cfd1d64d92e6563bbde02') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
# RUN php composer-setup.php
# RUN php -r "unlink('composer-setup.php');"
# RUN mv composer.phar /usr/local/bin/composer
RUN chmod 755 /var/www/html -R \
    && chown www-data:www-data /var/www/html

RUN a2enmod rewrite


ADD 000-default.conf /etc/apache2/sites-available/000-default.conf

# Set working directory
WORKDIR /var/www/html

COPY . /var/www/html

RUN composer install
RUN composer update

EXPOSE 80

