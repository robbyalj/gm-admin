@extends('layouts.auth')

@section('content')
<div class="container-fluid text-center mt-5">
	<h1 style="font-size: 18em">401</h1>
	<h2>Unauthorized</h2>
    <p>Anda tidak diizinkan untuk mengakses tindakan yang dipilih</p>
    <a href="{{ url('') }}" class="btn btn-primary">Kembali ke halaman awal</a>
</div>