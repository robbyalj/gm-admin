@extends('layouts.app')
@section('title', 'Pengaturan')

@section('content')
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 align-self-center">
            <h4 class="page-title text-truncate text-dark font-weight-medium mb-1">Pengaturan</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item"><a href="{{ url('') }}" class="text-muted">Home</a></li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Pengaturan</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    
    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <!-- basic table -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                	<h4 class="card-title">Pengaturan Kata Sandi</h4>
                    <form action="{{ url('changepassword') }}" method="POST" id="addForm" enctype="multipart/form-data">
                    @csrf
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Kata Sandi Sekarang <span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" name="currpass" id="currpass" placeholder="Masukan kata sandi sekarang">
                                </div>
                            </div>
                            <div class="col-md-12"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Kata Baru <span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Masukan kata sandi baru">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Ulang Kata Baru <span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" name="repass" id="repass" placeholder="Masukan ulang kata sandi baru">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="reset" class="btn btn-secondary">Batal</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>

<script type="text/javascript">
    $("#addForm").validate( {
        rules: {
            currpass: {
            	required: true,
            	minlength: 6
            },
            password: {
            	required: true,
            	minlength: 6
            },
            repass: {
            	equalTo: "#password"
            }
        },
        messages: {
            currpass: {
            	required: "Kata sandi sekarang tidak boleh kosong",
            	minlength: "Minimal panjang 6 karakter"
            },
            password: {
            	required: "Kata sandi baru tidak boleh kosong",
            	minlength: "Minimal panjang 6 karakter"
            },
            repass: {
            	equalTo: "Ulang kata sandi baru salah"
            }
        },
        errorElement: "em",
        errorClass: "invalid-feedback",
        errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            $(element).parents('.form-group').append(error);
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).addClass("is-invalid").removeClass("is-valid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).addClass("is-valid").removeClass("is-invalid");
        }
    });
</script>
<!-- ============================================================== -->
<!-- End Container fluid  -->
</div>

@endsection