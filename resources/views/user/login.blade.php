@extends('layouts.auth')

@section('content')
<div class="auth-wrapper d-flex no-block justify-content-center align-items-center position-relative">
    <div class="auth-box row">
        <div class="col-lg-7 col-md-5 modal-bg-img" style="background-image: url({{ url('assets/images/login-image.svg') }});">
        </div>
        <div class="col-lg-5 col-md-7 bg-white">
            <div class="p-3 mt-5">
                <h2 class="mt-3 text-center">GM Tactical Admin </h2>
                <p class="text-center">Masukan Email dan Kata Sandi anda untuk memasuki admin panel.</p>
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                <form class="mt-4" method="POST" action="{{ url('do_login') }}">
                    @csrf
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="text-dark" for="email">Email</label>
                                <input class="form-control" id="email" type="text" name="email" placeholder="Masukan email">
                                @if ($errors->has('email'))
                                    <small class="text-danger">{{ $errors->first('email') }}</small>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="text-dark" for="password">Kata Sandi</label>
                                <input class="form-control" id="password" type="password" name="password" placeholder="Masukan kata sandi">
                                @if ($errors->has('password'))
                                    <small class="text-danger">{{ $errors->first('password') }}</small>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-12 text-center">
                            <button type="submit" class="btn btn-block btn-dark">Masuk</button>
                        </div>
                        <div class="col-lg-12 text-center mt-5">
                            <small class="text-muted">&copy;2021. GM Tactical Admin <br></small>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>