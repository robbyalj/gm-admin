<?php use App\Helpers\MenuHelper; ?>

<nav class="sidebar-nav">
    <ul id="sidebarnav">
        {!! MenuHelper::render() !!}                
    </ul>
</nav>