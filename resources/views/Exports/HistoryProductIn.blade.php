<table border="1">
	<caption>Data Barang Masuk</caption>
	<thead>
		<tr>
			<th align="center">No</th>
			<th align="center">Nama</th>
            <th align="center">Varian</th>
            <th align="center">Ukuran</th>
			<th align="center">Jumlah Input</th>
			<th align="center">Jumlah Akhir</th>
            <th align="center">Waktu</th>
            <th align="center">User</th>
		</tr>
	</thead>
	<tbody>
		@if (sizeof($products) == 0)
           	<tr>
                <td colspan="6" align="center">Data kosong</td>
            </tr>
        @else
            @foreach ($products as $product)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $product->product_name }}</td>
                    <td>{{ $product->product_variant }}</td>
                    <td>{{ $product->product_size }}</td>
                    <td>{{ $product->input_stock_a }}</td>
                    <td>{{ $product->current_stock_a }}</td>
                    <td>{{ $product->created_at }}</td>
                    <td>{{ $product->name }}</td>
                </tr>
            @endforeach
        @endif
	</tbody>
</table>			
